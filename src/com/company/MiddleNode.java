package com.company;

public class MiddleNode {
    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public ListNode middleNode(ListNode head) {
        ListNode slow = head;
        ListNode fast = head;

        if(head != null){
            while(fast != null && fast.next != null){
                slow = slow.next;
                fast = fast.next.next;
            }
        }

        return slow;
    }
}
