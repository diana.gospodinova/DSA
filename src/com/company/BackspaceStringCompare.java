package com.company;

import java.util.Stack;

public class BackspaceStringCompare {
    public static boolean backspaceCompare(String firstString, String secondString) {
        Stack<Character> firstResult = new Stack<>();
        Stack<Character> secondResult = new Stack<>();

        for (int i = 0; i < firstString.length(); i++) {
            char c = firstString.charAt(i);
            if (c == '#') {
                if (firstResult.size() > 0) {
                    firstResult.pop();
                }
            } else {
                firstResult.push(c);
            }
        }

        for (int i = 0; i < secondString.length(); i++) {
            char c = secondString.charAt(i);
            if (c == '#') {
                if (secondResult.size() > 0) {
                    secondResult.pop();
                }
            } else {
                secondResult.push(c);
            }
        }
        return firstResult.equals(secondResult);
    }
}
