package com.company;

import java.util.ArrayDeque;

public class BaseballGame {
    public static int calPoints(String[] ops) {
        ArrayDeque<Integer> rounds = new ArrayDeque<>();
        int result = 0;

        for (int i = 0; i < ops.length; i++) {
            switch (ops[i]) {
                case "+":
                    if (rounds.size() >= 2) {
                        int temp = rounds.peekLast();
                        int first = temp;
                        rounds.pollLast();
                        temp = rounds.peekLast();
                        int second = temp;
                        int round = first + second;
                        result = result + round;
                        rounds.addLast(first);
                        rounds.addLast(round);
                    }
                    break;
                case "D":
                    if (rounds.size() > 0) {
                        rounds.addLast(2 * rounds.peekLast());
                        result = result + rounds.peekLast();
                    }
                    break;
                case "C":
                    if (rounds.size() > 0) {
                        result = result - rounds.peekLast();
                        rounds.pollLast();
                    }
                    break;
                default:
                    result = result + Integer.parseInt(ops[i]);
                    rounds.addLast(Integer.parseInt(ops[i]));
                    break;
            }
        }
        return result;
    }
}
