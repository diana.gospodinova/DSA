package com.company;

import org.apache.commons.lang3.ArrayUtils;

public class NextGreaterElement {
    public static int[] nextGreaterElement(int[] nums1, int[] nums2) {
        int[] result = new int[nums1.length];
        for (int i = 0; i < nums1.length; i++) {
            for (int j = 0; j < nums2.length - 1; j++) {
                if (nums1[i] == nums2[j]) {
                    for (int k = j + 1; k < nums2.length; k++) {
                        if (nums1[i] < nums2[k]) {
                            result[i] = nums2[k];
                            break;
                        } else {
                            result[i] = -1;
                        }
                    }
                    break;
                } else {
                    result[i] = -1;
                }

            }
        }
        return result;
    }
}
