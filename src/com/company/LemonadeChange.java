package com.company;

import java.util.LinkedList;

public class LemonadeChange {
    public static boolean lemonadeChange(int[] bills) {
        LinkedList<Integer> paidBills = new LinkedList<>();
        int availableAmount = 0;
        boolean isChangeCorrect = true;

        for (int i = 0; i < bills.length; i++) {
            if (isChangeCorrect) {
                switch (bills[i]) {
                    case 5:
                        isChangeCorrect = true;
                        break;
                    case 10:
                        if (availableAmount >= 5) {
                            paidBills.removeFirstOccurrence(5);
                            isChangeCorrect = true;
                        } else {
                            isChangeCorrect = false;
                        }
                        break;
                    case 20:
                        if (availableAmount >= 15) {
                            if (paidBills.contains(10)) {
                                if (paidBills.contains(5)) {
                                    paidBills.removeFirstOccurrence(10);
                                    paidBills.removeFirstOccurrence(5);
                                    isChangeCorrect = true;
                                } else {
                                    isChangeCorrect = false;
                                }
                            } else {
                                for (int j = 0; j < 3; j++) {
                                    if (paidBills.contains(5)) {
                                        paidBills.removeFirstOccurrence(5);
                                        isChangeCorrect = true;
                                    } else {
                                        isChangeCorrect = false;
                                        break;
                                    }
                                }
                            }
                        } else {
                            isChangeCorrect = false;
                        }
                        break;
                }
                if (isChangeCorrect) {
                    paidBills.add(bills[i]);
                    availableAmount += 5;
                }
            }
        }
        return isChangeCorrect;
    }
}
