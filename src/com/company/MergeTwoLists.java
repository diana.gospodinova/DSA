package com.company;

public class MergeTwoLists {
    public class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode head = new ListNode(0);
        ListNode result = head;
        ListNode first = l1;
        ListNode second = l2;

        while(true) {
            if(first == null){
                result.next = second;
                break;
            }

            if(second == null){
                result.next = first;
                break;
            }

            if (first.val <= second.val) {
                result.next = first;
                first = first.next;
            } else {
                result.next = second;
                second = second.next;
            }

            result = result.next;
        }

        return head.next;
    }
}
