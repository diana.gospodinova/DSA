package com.company;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import static com.company.AsteroidCollision.asteroidCollision;
import static com.company.BackspaceStringCompare.backspaceCompare;
import static com.company.BaseballGame.calPoints;
import static com.company.LemonadeChange.lemonadeChange;
import static com.company.NextGreaterElement.nextGreaterElement;
import static com.company.TheChallenge.extractParenthesisExpressions;
import static com.company.ValidParentheses.isValid;

public class Main {

    public static void main(String[] args) {

        /*String input = "{[]}";
        System.out.println(isValid(input));

        String firstInput = "y#fo##f";
        String secondInput = "y#f#o##f";
        System.out.println(backspaceCompare(firstInput, secondInput));
*/
        /*int[] bills = {5,5,20,5,5,10,5,10,5,20};
        System.out.println(lemonadeChange(bills));*/

        /*ImplementQueueUsingStacks queue = new ImplementQueueUsingStacks();
        queue.push(1);
        queue.push(2);
        queue.peek();  // returns 1
        queue.pop();   // returns 1
        queue.empty(); // returns false*/

       /* int[] nums1 = {1, 3, 5, 2, 4};
        int[] nums2 = {6, 5, 4, 3, 2, 1, 7};
        System.out.println(Arrays.toString(nextGreaterElement(nums1, nums2)));*/

        String expression = "1 + (2 - (2 + 3) * 4 / (3 + 1)) * 5";
        extractParenthesisExpressions(expression);

        //int[] asteroids = {-2,2,-1,-2};
       /* int[] asteroids = {-2,-2,1,-2};
        System.out.println(Arrays.toString(asteroidCollision(asteroids)));*/

       /* String[] ops2 = {"5", "-2", "4", "C", "D", "9", "+", "+"};
        String[] ops = {"5","2","C","D","+"};
        System.out.println(calPoints(ops2));*/

        /*Set<Integer> intSet = new HashSet<>(Arrays.asList(12, 2, 6, 14, 8, 1, 5, 3, 12, 5));
        int sum = 24;

        for (int number: intSet) {
            *//*if(intSet.contains(sum - number)){
                System.out.println(String.format("The first pair is: %d & %d", number, sum - number));
                break;
            }else{
                System.out.println("13 is not your lucky number :(");
            }*//*
            if(intSet.contains(sum - number)){
                System.out.println(String.format("The first pair is: %d & %d", number, sum - number));
                return;
            }
        }

        System.out.println("Doesn't exist.");*/
    }
}
