package com.company;

import java.util.Stack;

public class ImplementQueueUsingStacks<T> {
    private Stack<Integer> myQueue;

    /**
     * Initialize your data structure here.
     */
    public ImplementQueueUsingStacks() {
        myQueue = new Stack<>();
    }

    /**
     * Push element x to the back of queue.
     */
    public void push(int x) {
        myQueue.add(x);
    }

    /**
     * Removes the element from in front of queue and returns that element.
     */
    public int pop() {
        if (myQueue.isEmpty()) {
            throw new IllegalArgumentException();
        }
        return myQueue.remove(0);
    }

    /**
     * Get the front element.
     */
    public int peek() {
        if (myQueue.isEmpty()) {
            throw new IllegalArgumentException();
        }
        return myQueue.firstElement();
    }

    /**
     * Returns whether the queue is empty.
     */
    public boolean empty() {
        return myQueue.size() == 0;
    }
}
