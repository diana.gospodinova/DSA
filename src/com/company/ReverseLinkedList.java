package com.company;

import java.util.List;

public class ReverseLinkedList {
    // TODO Debug the solution
    public static class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }

    public static ListNode reverseList(ListNode head) {
        ListNode prev = null;
        ListNode current = head;

        while (current != null) {
            ListNode nextTemp = current.next;
            current.next = prev;
            prev = current;
            current = nextTemp;
        }

        return prev;
    }

    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);

        ListNode current = head;
        while (current != null) {
            System.out.println(current.val);
            current = current.next;
        }

        System.out.println();

        ListNode list = reverseList(head);

        while (list != null) {
            System.out.println(list.val);
            list = list.next;
        }

    }
}
