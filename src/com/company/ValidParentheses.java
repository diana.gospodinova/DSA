package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class ValidParentheses {
    public static boolean isValid(String input) {
        Stack<Character> parenthesisOccurrence = new Stack<>();
        ArrayList<Character> openingParenthesis = new ArrayList<>(Arrays.asList('(', '{', '['));
        ArrayList<Character> closingParenthesis = new ArrayList<>(Arrays.asList(')', '}', ']'));

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (parenthesisOccurrence.size() > 0
                    && closingParenthesis.contains(c)
                    && parenthesisOccurrence.peek().equals(openingParenthesis.get(closingParenthesis.indexOf(c)))) {
                parenthesisOccurrence.pop();
            } else {
                parenthesisOccurrence.push(c);
            }

        }
        return parenthesisOccurrence.isEmpty();
    }
}
