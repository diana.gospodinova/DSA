package com.company;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Stack;

public class TheChallenge {
    public static void extractParenthesisExpressions(String expression) {
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("JavaScript");

        Stack<Character> parenthesisOccurrence = new Stack<>();
        Stack<Integer> parenthesisIndexes = new Stack<>();

        ArrayList<Character> openingParenthesis = new ArrayList<>(Arrays.asList('(', '{', '['));
        ArrayList<Character> closingParenthesis = new ArrayList<>(Arrays.asList(')', '}', ']'));

        for (int i = 0; i < expression.length(); i++) {
            char c = expression.charAt(i);
            if (openingParenthesis.contains(c)) {
                parenthesisOccurrence.push(c);
                parenthesisIndexes.push(i);
            } else {
                if (parenthesisOccurrence.size() > 0
                        && closingParenthesis.contains(c)
                        && parenthesisOccurrence.peek().equals(openingParenthesis.get(closingParenthesis.indexOf(c)))) {
                    int startIndex = parenthesisIndexes.peek();

                    String result = expression.substring(startIndex, i + 1);

                    try {
                        System.out.println(result + " => " + engine.eval(result));
                    } catch (ScriptException e) {
                        e.printStackTrace();
                    }
                    parenthesisOccurrence.pop();
                    parenthesisIndexes.pop();
                }
            }
        }

        try {
            System.out.println(expression + " => " + engine.eval(expression));
        } catch (ScriptException e) {
            e.printStackTrace();
        }

    }
}
