package com.company;

import java.util.ArrayDeque;

public class AsteroidCollision {
    public static int[] asteroidCollision(int[] asteroids) {
        ArrayDeque<Integer> asteroid = new ArrayDeque<>();
        if (asteroids.length > 0) {

            for (int i = 0; i < asteroids.length; i++) {
                if(asteroid.size() == 0){
                    asteroid.addLast(asteroids[i]);
                    continue;
                }

                if (asteroids[i] > 0) {
                    asteroid.addLast(asteroids[i]);
                    continue;
                }

                if (asteroids[i] < 0 && asteroid.peekLast() < 0) {
                    asteroid.addLast(asteroids[i]);
                    continue;
                }

                if (asteroids[i] < 0 && asteroid.peekLast() + asteroids[i] == 0) {
                    asteroid.pollLast();
                    continue;
                }

                if (asteroids[i] < 0 && asteroid.peekLast() > 0) {
                    if (asteroids[i] + asteroid.peekLast() > 0) {
                        continue;
                    } else {
                        while (asteroid.size() > 0 && asteroid.peekLast() > 0 && asteroid.peekLast() + asteroids[i] < 0) {
                            asteroid.pollLast();
                        }

                        if(asteroid.size() == 0){
                            asteroid.addLast(asteroids[i]);
                            continue;
                        }

                        if(asteroid.peekLast() < 0){
                            asteroid.addLast(asteroids[i]);
                            continue;
                        }

                        if (asteroid.peekLast() + asteroids[i] == 0 && asteroid.size() > 0) {
                            asteroid.pollLast();
                            continue;
                        }

                        if (asteroid.peekLast() + asteroids[i] > 0) {
                            continue;
                        }
                    }
                }
            }
        }

        return asteroid.stream()
                .mapToInt(element -> element)
                .toArray();
    }
}

