package dsa_exam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class JediMeditation {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        int jediNumber = Integer.parseInt(userInput.readLine());
        String[] jedis = userInput.readLine().split(" ");

        StringBuilder masters = new StringBuilder();
        StringBuilder knights = new StringBuilder();
        StringBuilder padawans = new StringBuilder();

        for (String jedi : jedis) {
            if (jedi.charAt(0) == 'M') {
                System.out.print(jedi + " ");
                continue;
            }

            if (jedi.charAt(0) == 'K') {
                knights.append(jedi + " ");
                continue;
            }

            padawans.append(jedi + " ");
        }

        //System.out.print(masters);
        System.out.print(knights);
        System.out.println(padawans.toString().trim());
    }
}
