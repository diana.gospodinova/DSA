package dsa_exam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PlusOneMultipleOne {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        String[] members = userInput.readLine().split(" ");
        int startNumber = Integer.parseInt(members[0]);
        int membersCount = Integer.parseInt(members[1]);
        int[] results = new int[membersCount];
        results[0] = startNumber;
        int base;

        for(int i = 1; i < membersCount; i++){
            if(i % 3 == 0){
                base = results[(i / 3) - 1];
            }else{
                base = results[i / 3];
            }
            //System.out.print(base + "  ");
            if(i % 3 == 1){
                results[i] = base + 1;
                //System.out.println( results[i]);
                continue;
            }
            if(i % 3 == 2){
                results[i] = 2 * base + 1;
                //System.out.println( results[i]);
                continue;
            }
            if(i % 3 == 0){
                results[i] = base + 2;
                //System.out.println( results[i]);
                continue;
            }
        }

        System.out.println(results[Integer.parseInt(members[1]) - 1]);
    }
}
