package dsa_exam;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.stream.Collectors;

public class OrderSystem {
    static class Order implements Comparable<Order> {
        String name;
        Double price;
        String consumer;

        public Order(String name, Double price, String consumer) {
            this.name = name;
            this.price = price;
            this.consumer = consumer;
        }

        public Double getPrice() {
            return price;
        }

        public String getName() {
            return name;
        }

        @Override
        public String toString() {
            return String.format("{%s;%s;%.2f}", name, consumer, price);
        }

        @Override
        public int compareTo(Order order) {
            return this.name.compareTo(order.name);
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        int commands = Integer.parseInt(userInput.readLine());
        //Comparator<Order> compareByName = Comparator.comparing(Order::getName);
        ArrayList<Order> orders = new ArrayList<>();
        HashMap<String, ArrayList<Order>> consumers = new HashMap<>();

        String commandParameters;
        String[] parameters;
        String name;
        Double price;
        String consumer;
        int count;

        while (commands > 0) {
            commandParameters = userInput.readLine();

            int firstDelimiter = commandParameters.indexOf(" ");
            String command = commandParameters.substring(0, firstDelimiter);
            commandParameters = commandParameters.replace(command, "").trim();

            switch (command) {
                case "AddOrder":
                    parameters = commandParameters.split(";");
                    name = parameters[0];
                    price = Double.parseDouble(parameters[1]);
                    consumer = parameters[2];
                    Order newOrder = new Order(name, price, consumer);
                    if (consumers.get(consumer) == null) {
                        consumers.put(consumer, new ArrayList<>());
                    }
                    orders.add(newOrder);
                    consumers.get(consumer).add(newOrder);
                    System.out.println("Order added");
                    commands--;
                    break;
                case "DeleteOrders":
                    consumer = commandParameters;
                    count = 0;
                    if (consumers.get(consumer) != null) {
                        for (Order order : consumers.get(consumer)) {
                            orders.remove(order);
                            count++;
                        }
                        consumers.remove(consumer);
                        System.out.println(String.format("%s orders deleted", count));
                    }else{
                        System.out.println("No orders found");
                    }
                    commands--;
                    break;
                case "FindOrdersByPriceRange":
                    parameters = commandParameters.split(";");
                    Double startPrice = Double.parseDouble(parameters[0]);
                    Double endPrice = Double.parseDouble(parameters[1]);
                    String result;
                    result = orders
                            .stream()
                            .filter(order -> order.getPrice() >= startPrice && order.getPrice() <= endPrice)
                            .sorted()
                            .map(Order::toString)
                            .collect(Collectors.joining("\n"));
                    if (result.equals("")) {
                        System.out.println("No orders found");
                    } else {
                        System.out.println(result);
                    }
                    commands--;
                    break;
                case "FindOrdersByConsumer":
                    consumer = commandParameters;
                    if (consumers.get(consumer) != null) {
                        System.out.println(consumers.get(consumer)
                                .stream()
                                .sorted()
                                .map(Order::toString)
                                .collect(Collectors.joining("\n")));
                    } else {
                        System.out.println("No orders found");
                    }
                    commands--;
                    break;
            }
        }
    }
}
