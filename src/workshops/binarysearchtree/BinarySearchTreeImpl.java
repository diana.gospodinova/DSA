package workshops.binarysearchtree;

import java.util.*;

public class BinarySearchTreeImpl implements BinarySearchTree, Iterable<Integer> {
    private BinaryTreeNode root;
    List<Integer> result = new ArrayList<>();

    public BinarySearchTreeImpl() {
        root = null;
    }

    public BinarySearchTreeImpl(BinaryTreeNode root) {
        this.root = root;
    }

    @Override
    public BinaryTreeNode getRoot() {
        return root;
    }

    @Override
    public void insert(int value) {
        root = insertRec(root, value);
    }

    @Override
    public BinaryTreeNode search(int value) {
        return searchNode(root, value);
    }

    @Override
    public List<Integer> inOrder() {
        result = new ArrayList<>();
        return inorderRec(root);
    }

    @Override
    public List<Integer> postOrder() {
        result = new ArrayList<>();
        return postOrderRec(root);
    }

    @Override
    public List<Integer> preOrder() {
        result = new ArrayList<>();
        return preOrderRec(root);
    }

    @Override
    public List<Integer> bfs() {
        result = new ArrayList<>();
        return bfsList(root);
    }

    @Override
    public int height() {
        return maxHeight(root) - 1;
    }

    @Override
    public BinaryTreeNode remove(int value) {
        root = removeNode(root, value);
        return root;
    }

    private BinaryTreeNode insertRec(BinaryTreeNode root, int value) {
        if (root == null) {
            root = new BinaryTreeNode(value);
            return root;
        }

        if (value < root.getValue()) {
            root.setLeftChild(insertRec(root.getLeftChild(), value));
        } else if (value > root.getValue()) {
            root.setRightChild(insertRec(root.getRightChild(), value));
        }
        return root;
    }

    private BinaryTreeNode searchNode(BinaryTreeNode root, int value) {
        if (root == null || root.getValue() == value) {
            return root;
        }

        if (root.getValue() > value) {
            return searchNode(root.getLeftChild(), value);
        }

        return searchNode(root.getRightChild(), value);
    }

    private List<Integer> inorderRec(BinaryTreeNode root) {
        if (root != null) {
            inorderRec(root.getLeftChild());
            result.add(root.getValue());
            inorderRec(root.getRightChild());
        }
        return result;
    }

    private List<Integer> postOrderRec(BinaryTreeNode root) {
        if (root != null) {
            postOrderRec(root.getLeftChild());
            postOrderRec(root.getRightChild());
            result.add(root.getValue());
        }
        return result;
    }

    private List<Integer> preOrderRec(BinaryTreeNode root) {
        if (root != null) {
            result.add(root.getValue());
            preOrderRec(root.getLeftChild());
            preOrderRec(root.getRightChild());
        }
        return result;
    }

    private List<Integer> bfsList(BinaryTreeNode root) {
        if (root == null) {
            return result;
        }

        Queue<BinaryTreeNode> tree = new ArrayDeque<>();
        tree.offer(root);
        while (!tree.isEmpty()) {
            BinaryTreeNode nextNode = tree.poll();
            result.add(nextNode.getValue());
            if (nextNode.getLeftChild() != null)
                tree.add(nextNode.getLeftChild());
            if (nextNode.getRightChild() != null)
                tree.add(nextNode.getRightChild());
        }
        return result;
    }

    private int maxHeight(BinaryTreeNode root) {
        if (root == null) {
            return 0;
        } else {
            int leftSubtreeHeight = maxHeight(root.getLeftChild());
            int rightSubtreeHeight = maxHeight(root.getRightChild());

            if (leftSubtreeHeight > rightSubtreeHeight) {
                return leftSubtreeHeight + 1;
            } else {
                return rightSubtreeHeight + 1;
            }
        }
    }

    private BinaryTreeNode removeNode(BinaryTreeNode root, int value) {
        if (root == null) {
            return null;
        }

        if (root.getValue() == value) {
            return root;
        }

        if (searchNode(root, value) == null) {
            return null;
        }

        if (value < root.getValue()) {
            root.setLeftChild(removeNode(root.getLeftChild(), value));
        } else if (value > root.getValue()) {
            root = root.getRightChild();
            root.setRightChild(removeNode(root.getRightChild(), value));
        } else {
            if (root.getLeftChild() == null && root.getRightChild() == null) {
                return null;
            } else if (root.getLeftChild() == null) {
                return root.getRightChild();
            } else if (root.getRightChild() == null) {
                return root.getLeftChild();
            } else {
                Integer minValue = minValue(root.getRightChild());
                root.setValue(minValue);
                root.setRightChild(removeNode(root.getRightChild(), minValue));
            }
        }
        return root;
    }

    int minValue(BinaryTreeNode root) {
        int minvalue = root.getValue();
        while (root.getLeftChild() != null) {
            minvalue = root.getLeftChild().getValue();
            root = root.getLeftChild();
        }
        return minvalue;
    }

    @Override
    public Iterator<Integer> iterator() {
        return inOrder().iterator();
    }
}
