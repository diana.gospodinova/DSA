package workshops.binarysearchtree;

public class Main {
    public static void main(String[] args) {
        BinarySearchTreeImpl tree = new BinarySearchTreeImpl();

        tree.insert(8);
        tree.insert(7);
        tree.insert(5);
        tree.insert(9);
        tree.insert(11);
        tree.insert(2);

        System.out.println(tree.getRoot());
        System.out.println(tree.preOrder());
        System.out.println(tree.inOrder());
        System.out.println(tree.postOrder());
        System.out.println(tree.bfs());

        for (Integer node : tree) {
            System.out.println(node);
        }
    }
}
