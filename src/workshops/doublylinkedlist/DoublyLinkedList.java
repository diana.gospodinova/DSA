package workshops.doublylinkedlist;


public interface DoublyLinkedList<T> extends Iterable<T> {
    DoubleNode getHead();
    DoubleNode getTail();
    int getCount();
    public void addLast(T value);
    public void addFirst(T value);
    public void insertBefore(DoubleNode node, T value);
    public void insertAfter(DoubleNode node, T value);
    T removeFirst();
    T removeLast();
    DoubleNode find(T value);
}
