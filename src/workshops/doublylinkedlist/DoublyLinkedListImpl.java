package workshops.doublylinkedlist;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

public class DoublyLinkedListImpl<T> implements DoublyLinkedList<T> {
    private DoubleNode<T> head;
    private DoubleNode<T> tail;
    private int count = 0;

    public DoublyLinkedListImpl() {
        this.head = head;
        this.tail = tail;
    }

    public DoublyLinkedListImpl(List<T> elements) {
        for (int i = 0; i < elements.size(); i++) {
            addLast(elements.get(i));
        }
    }

    @Override
    public DoubleNode getHead() {
        if (count == 0) {
            throw new IllegalArgumentException("No elements in the list.");
        }
        return head;
    }

    @Override
    public DoubleNode getTail() {
        if (count == 0) {
            throw new IllegalArgumentException("No elements in the list.");
        }
        return tail;
    }

    @Override
    public int getCount() {
        return count;
    }

    @Override
    public void addLast(T value) {
        DoubleNode<T> last = new DoubleNode<>(value);
        if (count == 0) {
            head = tail = last;
        }

        if (count == 1) {
            head.next = last;
            last.prev = head;
            tail = last;
        }

        if (count > 1) {
            tail.next = last;
            last.prev = tail;
            tail = last;
        }

        count++;
    }

    @Override
    public void addFirst(T value) {
        DoubleNode<T> first = new DoubleNode<>(value);
        if (count == 0) {
            head = tail = first;
        }

        if (count == 1) {
            tail.prev = first;
            first.next = tail;
            head = first;
        }

        if (count > 1) {
            head.prev = first;
            first.next = head;
            head = first;
        }

        count++;
    }

    @Override
    public void insertBefore(DoubleNode node, T value) {
        DoubleNode<T> newNode = new DoubleNode<>(value);
        if (count == 0) {
            throw new NullPointerException("No elements in the list.");
        }

        if (count == 1) {
            addFirst(value);
            return;
        }

        if (node.prev == null) {
            addFirst(value);
            return;
        }

        node.prev.next = newNode;
        newNode.prev = node.prev;
        newNode.next = node;
        node.prev = newNode;

        count++;
    }

    @Override
    public void insertAfter(DoubleNode node, T value) {
        DoubleNode<T> newNode = new DoubleNode<>(value);
        if (count == 0) {
            throw new NullPointerException("No elements in the list.");
        }

        if (count == 1) {
            addLast(value);
            return;
        }

        if (node.next == null) {
            addLast(value);
            return;
        }

        node.next.prev = newNode;
        newNode.next = node.next;
        newNode.prev = node;
        node.next = newNode;

        count++;
    }

    @Override
    public T removeFirst() {
        if (count == 0) {
            throw new NullPointerException("No elements in the list.");
        }

        DoubleNode<T> temp = head;

        if (count > 1) {
            head = head.next;
            head.prev = null;
        } else {
            head = null;
            tail = null;
        }
        count--;

        return temp.getValue();
    }

    @Override
    public T removeLast() {
        if (count == 0) {
            throw new NullPointerException("No elements in the list.");
        }

        DoubleNode<T> temp = tail;

        if (count > 1) {
            tail = tail.prev;
            tail.next = null;
        } else {
            head = null;
            tail = null;
        }

        count--;

        return temp.getValue();
    }

    @Override
    public DoubleNode find(T value) {
        int i = 1;
        DoubleNode<T> current = head;
        DoubleNode<T> result = null;

        while (current != null) {
            if (current.getValue() == value) {
                result = current;
                break;
            }
            current = current.next;
            i++;
        }

        return result;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            DoubleNode<T> current = head;

            @Override
            public T next() {
                T data = current.getValue();
                current = current.getNext();

                return data;
            }

            @Override
            public boolean hasNext() {
                return current != null;
            }
        };
    }
}
