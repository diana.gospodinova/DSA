package workshops.doublylinkedlist;

import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {

        //Test LinkedList
        LinkedList testList = new LinkedList();
        testList.add(0, 6);
        testList.add(1, 13);
        System.out.println(testList.size());

        DoublyLinkedListImpl<String> myList = new DoublyLinkedListImpl<>();

        myList.addFirst("abc");
        myList.addFirst("mno");
        myList.addFirst("pqr");
        myList.addFirst("xyz");

        for (String string : myList) {
            System.out.println(string);
        }

        DoublyLinkedListImpl<String> myList2 = new DoublyLinkedListImpl<>();
        for (String string : myList2) {
            System.out.println(string);
        }
    }
}
