package workshops.recursion;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Fibonacci {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(fibonacciRec(Integer.parseInt(reader.readLine())));
    }

    static int fibonacciRec(int n) {
        if ((n == 0) || (n == 1))
            return n;

        else

        return fibonacciRec(n - 1) + fibonacciRec(n - 2);
    }
}
