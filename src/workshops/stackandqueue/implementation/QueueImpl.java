package workshops.stackandqueue.implementation;

import java.util.ArrayDeque;
import java.util.Deque;

public class QueueImpl<T> implements Queue<T> {
    // https://docs.oracle.com/javase/8/docs/api/java/util/Queue.html
    // https://docs.oracle.com/javase/8/docs/api/java/util/Deque.html
    private Deque<T> dequeStore;

    public QueueImpl() {
        dequeStore = new ArrayDeque<>();
    }

    // TODO Ask about the explanation of the method
    /* offer() - This method inserts the specified element into this queue
          if it is possible to do so immediately without violating capacity restrictions.
          It does not throws an exception when the capacity of the container is full since it returns false.*/
    @Override
    public void offer(T element) {
        try {
            dequeStore.offerLast(element);
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format("The element %s can't be added to the queue.", element));
        }
    }

    // poll()- This method removes and returns the head of the queue. It throws IllegalArgumentException if the queue is empty.
    @Override
    public T poll() {
        if (dequeStore.isEmpty()) {
            throw new IllegalArgumentException("The queue is empty.");
        }
        return dequeStore.pollFirst();
    }

    // isEmpty() - This method is used to check if the queue has elements.
    @Override
    public boolean isEmpty() {
        return (dequeStore.size() == 0);
    }

    // peek()- This method is used to view the head of queue without removing it.
    // It throws IllegalArgumentException if the queue is empty.
    @Override
    public T peek() {
        if (dequeStore.isEmpty()) {
            throw new IllegalArgumentException("The queue is empty.");
        } else {
            return this.dequeStore.peekFirst();
        }
    }

    // size()- This method return the no. of elements in the queue.
    @Override
    public int size() {
        return dequeStore.size();
    }
}
