package workshops.stackandqueue.implementation;

import java.util.ArrayDeque;
import java.util.Deque;

public class StackImpl<T> implements Stackable<T> {
    // https://docs.oracle.com/javase/8/docs/api/java/util/Queue.html
    // https://docs.oracle.com/javase/8/docs/api/java/util/Deque.html
    private Deque<T> dequeStore;


    public StackImpl() {
        dequeStore = new ArrayDeque<>();
    }

    // push() - This method adds an item in the stack. If the stack is full, then it is said to be an Overflow condition.
    @Override
    public void push(T element) {
        try {
            dequeStore.offerLast(element);
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format("The element %s can't be added to the stack.", element));
        }
    }

    // pop() - This method Removes an item from the stack. The items are popped in the reversed order in which they are pushed.
    // If the stack is empty, then it is said to be an Underflow condition.
    @Override
    public T pop() {
        if (dequeStore.isEmpty()) {
            throw new IllegalArgumentException("The stack is empty");
        }
        return dequeStore.pollLast();
    }

    // peek()- This method is used to view the top element of the stack without removing it.
    // It throws IllegalArgumentException if the stack is empty.
    @Override
    public T peek() {
        if (dequeStore.isEmpty()) {
            throw new IllegalArgumentException("The stack is empty.");
        } else {
            return this.dequeStore.peekLast();
        }
    }

    // size()- This method return the no. of elements in the stack.
    @Override
    public int size() {
        return dequeStore.size();
    }

    // isEmpty() - This method is used to check if the stack has elements.
    @Override
    public boolean isEmpty() {
        return (dequeStore.size() == 0);
    }
}
