package workshops.stackandqueue.implementation;

public interface Stackable<T> {
    void push(T element);

    T pop();

    T peek();

    int size();

    boolean isEmpty();
}
