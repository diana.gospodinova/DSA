package workshops.stackandqueue.implementation;

import java.util.LinkedList;

public class StackImplLL<T> implements Stackable<T> {
    private LinkedList<T> linkedList;

    public StackImplLL() {
        linkedList = new LinkedList();
    }

    @Override
    public void push(T element) {
        try {
            linkedList.addLast(element);
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format("The element %s can't be added to the stack.", element));
        }
    }

    @Override
    public T pop() {
        if(linkedList.isEmpty()){
            throw new IllegalArgumentException("The stack is empty.");
        }
        return (T) linkedList.removeLast();
    }

    @Override
    public T peek() {
        if(linkedList.isEmpty()){
            throw new IllegalArgumentException("The stack is empty.");
        }
        return (T) linkedList.getLast();
    }

    @Override
    public int size() {
        return linkedList.size();
    }

    @Override
    public boolean isEmpty() {
        return linkedList.size() == 0;
    }
}
