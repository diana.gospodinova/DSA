package workshops.stackandqueue.implementation;

public interface Queue<T> {
    void offer(T element);

    T poll();

    boolean isEmpty();

    T peek();

    int size();
}
