package workshops.stackandqueue.implementation;


import java.util.LinkedList;

public class QueueImplLL<T> implements Queue<T>{
    private LinkedList<T> linkedList;

    public QueueImplLL() {
        linkedList = new LinkedList();
    }

    @Override
    public void offer(T element) {
        try {
            linkedList.addLast(element);
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format("The element %s can't be added to the queue.", element));
        }
    }

    @Override
    public T poll() {
        if(linkedList.isEmpty()){
            throw new IllegalArgumentException("The queue is empty.");
        }
        return linkedList.removeFirst();
    }

    @Override
    public boolean isEmpty() {
        return linkedList.size() == 0;
    }

    @Override
    public T peek() {
        if(linkedList.isEmpty()){
            throw new IllegalArgumentException("The queue is empty.");
        }
        return linkedList.getFirst();
    }

    @Override
    public int size() {
        return linkedList.size();
    }
}
