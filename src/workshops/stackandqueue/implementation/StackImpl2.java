package workshops.stackandqueue.implementation;

import java.util.Stack;

public class StackImpl2<T> implements Stackable<T> {
    // https://docs.oracle.com/javase/8/docs/api/java/util/Stack.html
    private Stack<T> stackStore;

    public StackImpl2() {
        stackStore = new Stack<>();
    }

    @Override
    public void push(T element) {
        try {
            stackStore.addElement(element);
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format("The element %s can't be added to the stack.", element));
        }

    }

    @Override
    public T pop() {
        if (stackStore.isEmpty()) {
            throw new IllegalArgumentException("The stack is empty");
        }
        return stackStore.remove(stackStore.size() - 1);
    }

    @Override
    public T peek() {
        if (stackStore.isEmpty()) {
            throw new IllegalArgumentException("The stack is empty.");
        } else {
            return stackStore.lastElement();
        }
    }

    @Override
    public int size() {
        return stackStore.size();
    }

    @Override
    public boolean isEmpty() {
        return (stackStore.size() == 0);
    }
}
