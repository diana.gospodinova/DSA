package workshops.stackandqueue.implementation;

public class QueueImplSLL<T> implements Queue<T> {
    private QNode head;
    private QNode last;
    private int size;

    @Override
    public void offer(T element) {
        /* 1 & 2: Allocate the Node &
              Put in the data*/
        QNode<T> new_node = new QNode(element);

        // If queue is empty, then new node is head and next both
        if (isEmpty()) {
            head = last = new_node;
        } else {
           last.next = new_node;
        }
        size++;
    }

    @Override
    public T poll() {
        if (isEmpty()) {
            throw new IllegalArgumentException("The queue is empty.");
        }
        QNode temp = head;
        head = head.next;

        size--;

        return (T) temp.key;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public T peek() {
        if (isEmpty()) {
            throw new IllegalArgumentException("The queue is empty.");
        }
        return (T) head.key;
    }

    @Override
    public int size() {
        return size;
    }

    // A linked list (LL) node to store a queue entry
    static class QNode<T> {
        T key;
        QNode next;

        // constructor to create a new linked list node
        public QNode(T key) {
            this.key = key;
            this.next = null;
        }
    }
}
