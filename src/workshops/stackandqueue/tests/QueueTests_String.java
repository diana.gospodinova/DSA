package workshops.stackandqueue.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import workshops.stackandqueue.implementation.Queue;
import workshops.stackandqueue.implementation.QueueImpl;
import workshops.stackandqueue.implementation.QueueImplLL;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class QueueTests_String {
    private Queue<String> testQueue;

    @Before
    public void before() {
        testQueue = new QueueImpl<>();
    }

   /* private Queue<String> testQueue;

    @Before
    public void before() {
        testQueue = new QueueImplLL();
    }*/

    @Test
    public void offer_should_addElement_when_QueueEmpty() {
        // Act
        testQueue.offer("First Element");
        // Assert
        Assert.assertEquals("First Element", testQueue.peek());
    }

    @Test
    public void offer_should_addElement_when_QueueNotEmpty() {
        // Arrange
        testQueue.offer("First Element");
        // Act
        testQueue.offer("Second Element");
        // Assert
        Assert.assertEquals("First Element", testQueue.peek());
    }

    @Test(expected = IllegalArgumentException.class)
    public void poll_should_throwException_when_QueueEmpty() {
        // Act&Assert
        testQueue.poll();
    }

    @Test
    public void poll_should_removeProperElement_when_QueueNotEmpty() {
        // Arrange
        testQueue.offer("First Element");
        testQueue.offer("Second Element");
        // Act
        String element = testQueue.poll();
        // Assert
        Assert.assertEquals("First Element", element);
    }

    @Test
    public void peek_should_returnProperElement_when_OnlyOneElementInQueue() {
        // Arrange
        testQueue.offer("First Element");
        // Act && Assert
        Assert.assertEquals("First Element", testQueue.peek());
    }

    @Test
    public void peek_should_returnProperElement_when_QueueHasMultipleElements() {
        // Arrange
        testQueue.offer("First Element");
        testQueue.offer("Second Element");
        testQueue.offer("Third Element");
        // Act && Assert
        Assert.assertEquals("First Element", testQueue.peek());
    }

    @Test(expected = IllegalArgumentException.class)
    public void peek_should_throwException_when_QueueEmpty() {
        // Act && Assert
        testQueue.peek();
    }

    @Test
    public void size_should_returnZero_when_QueueEmpty() {
        // Assert
        Assert.assertEquals(0, testQueue.size());
    }

    @Test
    public void size_should_returnProperSize_when_QueueHasMultipleElements() {
        testQueue.offer("First Element");
        testQueue.offer("Second Element");
        // Assert
        Assert.assertEquals(2, testQueue.size());
    }

    @Test
    public void isEmpty_should_returnTrue_when_QueueIsEmpty() {
        // Assert
        Assert.assertEquals(true, testQueue.isEmpty());
    }

    @Test
    public void isEmpty_should_returnFalse_when_QueueNotEmpty() {
        // Arrange
        testQueue.offer("First Element");
        // Assert
        Assert.assertEquals(false, testQueue.isEmpty());
    }
}
