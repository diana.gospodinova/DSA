package workshops.stackandqueue.tests;

import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import workshops.stackandqueue.implementation.StackImpl2;
import workshops.stackandqueue.implementation.StackImplLL;
import workshops.stackandqueue.implementation.Stackable;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class StackTests_String {
    private Stackable<String> testStack;

    @Before
    public void before() {
        testStack = new StackImpl2<>();
    }

    /*private Stackable<String> testStack;

    @Before
    public void before(){
        testStack = new StackImplLL();
    }*/

    @Test
    public void push_should_pushElement_when_StackEmpty() {
        // Act
        testStack.push("First Element");
        // Assert
        Assert.assertEquals("First Element", testStack.peek());
    }

    @Test
    public void push_should_pushElement_when_StackNotEmpty() {
        // Arrange
        testStack.push("First Element");
        // Act
        testStack.push("Second Element");
        // Assert
        Assert.assertEquals("Second Element", testStack.peek());
    }

    @Test(expected = IllegalArgumentException.class)
    public void pop_should_throwException_when_StackEmpty() {
        // Act
        testStack.pop();
    }

    @Test
    public void pop_should_removeCorrectElement_when_StackNotEmpty() {
        // Arrange
        testStack.push("First Element");
        testStack.push("Second Element");
        testStack.push("Third Element");
        // Act&&Assert
        Assert.assertEquals("Third Element", testStack.pop());
    }

    @Test(expected = IllegalArgumentException.class)
    public void peek_should_throwException_when_StackEmpty() {
        // Act
        testStack.peek();
    }

    @Test
    public void peek_should_returnCorrectValue_when_StackNotEmpty() {
        // Arrange
        testStack.push("First Element");
        testStack.push("Second Element");
        testStack.push("Third Element");
        // Act&&Assert
        Assert.assertEquals("Third Element", testStack.peek());
    }

    @Test
    public void size_should_returnZero_when_StackEmpty() {
        // Act&&Assert
        Assert.assertEquals(0, testStack.size());
    }

    @Test
    public void size_should_returnProperValue_when_StackNotEmpty() {
        // Arrange
        testStack.push("First Element");
        testStack.push("Second Element");
        testStack.push("Third Element");
        // Act&&Assert
        Assert.assertEquals(3, testStack.size());
    }

    @Test
    public void isEmpty_should_returnTrue_when_StackEmpty() {
        // Assert
        Assert.assertEquals(true, testStack.isEmpty());
    }

    @Test
    public void isEmpty_should_returnFalse_when_StackNotEmpty() {
        // Act
        testStack.push("First Element");
        // Assert
        Assert.assertEquals(false, testStack.isEmpty());
    }
}
