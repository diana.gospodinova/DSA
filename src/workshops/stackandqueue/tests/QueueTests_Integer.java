package workshops.stackandqueue.tests;

import com.company.LinkedList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import workshops.stackandqueue.implementation.Queue;
import workshops.stackandqueue.implementation.QueueImpl;
import workshops.stackandqueue.implementation.QueueImplLL;
import workshops.stackandqueue.implementation.QueueImplSLL;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class QueueTests_Integer {
   /* private Queue<Integer> testQueue;

    @Before
    public void before() {
        testQueue = new QueueImpl<>();;
    }*/

    /*private Queue<Integer> testQueue;

    @Before
    public void before() {
        testQueue = new QueueImplLL();
    }*/


    private Queue<Integer> testQueue;

    @Before
    public void before() {
        testQueue = new QueueImplSLL();
    }

    @Test
    public void offer_should_addElement_whenQueueEmpty() {
        // Act
        testQueue.offer(1);
        // Assert
        Assert.assertEquals(1, (int) testQueue.peek());
    }

    // TODO Ask about difference between offer and add
    /*@Test(expected = IllegalArgumentException.class)
    public void offer_should_throwException_when_AddInvalidElement() {
        // Act
        testQueue.offer(null);
    }*/

    @Test
    public void offer_should_addElement_when_QueueNotEmpty() {
        // Arrange
        testQueue.offer(6);
        // Act
        testQueue.offer(1);
        // Assert
        Assert.assertEquals(6, (int) testQueue.peek());
    }

    @Test(expected = IllegalArgumentException.class)
    public void poll_should_throwException_when_QueueEmpty() {
        // Act&Assert
        testQueue.poll();
    }

    @Test
    public void poll_should_removeProperElement_when_QueueNotEmpty() {
        // Arrange
        testQueue.offer(3);
        testQueue.offer(5);
        // Act
        int element = testQueue.poll();
        // Assert
        Assert.assertEquals(3, element);
    }

    @Test
    public void peek_should_returnProperElement_when_OnlyOneElementInQueue() {
        // Arrange
        testQueue.offer(1);
        // Act && Assert
        Assert.assertEquals(1, (int) testQueue.peek());
    }

    @Test
    public void peek_should_returnProperElement_when_QueueHasMultipleElements() {
        // Arrange
        testQueue.offer(4);
        testQueue.offer(5);
        testQueue.offer(6);
        // Act && Assert
        Assert.assertEquals(4, (int) testQueue.peek());
    }

    @Test(expected = IllegalArgumentException.class)
    public void peek_should_throwException_when_QueueEmpty() {
        // Act && Assert
        testQueue.peek();
    }

    @Test
    public void size_should_returnZero_when_QueueEmpty() {
        // Assert
        Assert.assertEquals(0, testQueue.size());
    }

    @Test
    public void size_should_returnProperSize_when_QueueHasMultipleElements() {
        // Arrange
        testQueue.offer(1);
        testQueue.offer(4);
        // Assert
        Assert.assertEquals(2, testQueue.size());
    }

    @Test
    public void isEmpty_should_returnTrue_when_QueueIsEmpty() {
        // Assert
        Assert.assertEquals(true, testQueue.isEmpty());
    }

    @Test
    public void isEmpty_should_returnFalse_when_QueueNotEmpty() {
        // Arrange
        testQueue.offer(2);
        // Assert
        Assert.assertEquals(false, testQueue.isEmpty());
    }
}
