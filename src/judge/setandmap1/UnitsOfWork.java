package judge.setandmap1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class UnitsOfWork {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        String[] commandParameters;
        String action;
        HashMap<String, Unit> units = new HashMap<>();
        TreeSet<Unit> unitsList = new TreeSet<>(new ComparatorByAttackAndName());
        HashMap<String, TreeSet<Unit>> typeLists = new HashMap<>();

        String name;
        String type;
        int attack;
        int numberOfUnits;

        while (true) {
            String command = userInput.readLine();
            if (command.equalsIgnoreCase("end")) {
                break;
            }
            commandParameters = command.split(" ");
            action = commandParameters[0];

            switch (action.toLowerCase()) {
                case "add":
                    name = commandParameters[1];
                    type = commandParameters[2];
                    attack = Integer.parseInt(commandParameters[3]);
                    if (units.putIfAbsent(name, new Unit(name, type, attack)) != null) {
                        System.out.println(String.format("FAIL: %s already exists!", name));
                    } else {
                        unitsList.add(units.get(name));
                        typeLists.putIfAbsent(type, new TreeSet<>(new ComparatorByAttackAndName()));
                        typeLists.get(type).add(units.get(name));
                        System.out.println(String.format("SUCCESS: %s added!", name));
                    }
                    break;
                case "remove":
                    name = commandParameters[1];
                    if (!units.keySet().contains(name)) {
                        System.out.println(String.format("FAIL: %s could not be found!", name));
                    } else {
                        type = units.get(name).type;
                        typeLists.get(type).remove(units.get(name));
                        unitsList.remove(units.get(name));
                        units.remove(name);
                        System.out.println(String.format("SUCCESS: %s removed!", name));
                    }
                    break;
                case "find":
                    type = commandParameters[1];
                    StringJoiner result = new StringJoiner(", ");
                    int count = 10;
                    if (typeLists.containsKey(type)) {
                        System.out.print("RESULT: ");
                        for (Unit unit : typeLists.get(type)) {
                            result.add(unit.toString());
                            count--;
                            if (count < 1) {
                                break;
                            }
                        }
                        System.out.println(result.toString().trim());
                       /* System.out.println(String.format("RESULT: %s", typeLists.get(type).stream()
                                .limit(10)
                                .map(Unit::toString)
                                .collect(Collectors.joining(", "))));*/
                    } else {
                        System.out.println("RESULT: ");
                    }
                    break;
                case "power":
                    numberOfUnits = Integer.parseInt(commandParameters[1]);
                    System.out.print("RESULT: ");
                    result = new StringJoiner(", ");
                    for (Unit unit : unitsList) {
                        result.add(unit.toString());
                        numberOfUnits--;
                        if (numberOfUnits < 1) {
                            break;
                        }
                    }
                    System.out.println(result.toString().trim());
                    /*System.out.println(String.format("RESULT: %s", unitsList.stream()
                            .limit(numberOfUnits)
                            .map(Unit::toString)
                            .collect(Collectors.joining(", "))));*/
                    break;
            }
        }
    }

    public static class Unit {
        private String name;
        private String type;
        private int attack;

        public Unit(String name, String type, int attack) {
            this.name = name;
            this.type = type;
            this.attack = attack;
        }


        @Override
        public String toString() {
            return String.format("%s[%s](%d)", name, type, attack);
        }
    }

    public static class ComparatorByAttackAndName implements Comparator<Unit> {
        @Override
        public int compare(Unit o1, Unit o2) {
            if (o1.attack == o2.attack) {
                return o1.name.compareTo(o2.name);
            } else {
                return o2.attack > o1.attack ? 1 : -1;
            }
        }
    }
}
