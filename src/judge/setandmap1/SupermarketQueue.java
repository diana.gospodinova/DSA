package judge.setandmap1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;

public class SupermarketQueue {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        String[] commandParameters;
        String action;
        List<String> people = new ArrayList<>();
        HashMap<String, Integer> names = new HashMap<>();
        String name;
        int position;
        int count;
        int counter = 0;
        StringJoiner result = new StringJoiner("\n");

        while (true) {
            String command = userInput.readLine();
            if (command.equals("End")) {
                System.out.println(result);
                break;
            }
            commandParameters = command.split(" ");
            action = commandParameters[0];

            switch (action) {
                case "Append":
                    name = commandParameters[1];
                    people.add(name);
                    if (names.containsKey(name)) {
                        names.put(name, (names.get(name) + 1));
                    } else {
                        names.put(name, 1);
                    }
                    counter++;
                    result.add("OK");
                    break;
                case "Insert":
                    position = Integer.parseInt(commandParameters[1]);
                    name = commandParameters[2];
                    try{
                        people.add(position, name);
                        if (names.containsKey(name)) {
                            names.put(name, (names.get(name) + 1));
                        } else {
                            names.put(name, 1);
                        }
                        counter++;
                        result.add("OK");
                    }catch (Exception ex){
                        result.add("Error");
                    }

                    break;
                case "Find":
                    count = 0;
                    name = commandParameters[1];
                    if (names.containsKey(name)) {
                        count = names.get(name);
                    }
                    result.add(String.valueOf(count));
                    break;
                case "Serve":
                    count = Integer.parseInt(commandParameters[1]);
                    StringJoiner serveResult = new StringJoiner(" ");
                    if (count > counter) {
                        result.add("Error");
                    } else {
                        for (int i = 0; i < count; i++) {
                            serveResult.add(people.get(i));
                            names.put(people.get(i), (names.get(people.get(i)) - 1));
                        }
                        people = people.subList(count, counter);
                        counter = counter - count;
                        result.add(String.valueOf(serveResult));
                    }
                    break;
            }
        }
    }
}
