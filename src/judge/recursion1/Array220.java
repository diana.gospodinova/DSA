package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Array220 {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        String[] input = userInput.readLine().split(",");
        int index = Integer.parseInt(userInput.readLine());

        //int[] numbers = Arrays.asList(input).stream().mapToInt(Integer::parseInt).toArray();
        System.out.println(nextDouble(input, index));
    }

    private static boolean nextDouble(String[] numbers, int index) {
        int length = numbers.length;
        boolean exists = false;
        if (index == length - 1 || exists)
            return false;

        if (Integer.parseInt(numbers[index]) * 10 == Integer.parseInt(numbers[index + 1])) {
            exists = true;
            return true;
        } else {
            return nextDouble(numbers, index + 1);
        }
    }
}
