package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Fibonacci {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        long number = Long.parseLong(userInput.readLine());
        System.out.println(fibonacci(number));
    }

    private static HashMap<Long, Long> fibonacciMap = new HashMap<Long, Long>();

    public static long fibonacci(long number) {
        if (number == 0 || number == 1) {
            return number;
        }

        try {
            // do not calculate anymore, just return it
            return fibonacciMap.get(number);
        } catch (NullPointerException np) {
            // has to be calculated once, then save it
            // calculation part
            long value = fibonacci(number - 1) + fibonacci(number - 2);
            // saving part
            fibonacciMap.put(number, value);
            return value;
        }
    }
}
