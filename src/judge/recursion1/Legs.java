package judge.recursion1;

public class Legs {
    public static void main(String[] args) {
        int[] legs = {5, 2, 7};
        int index = 0;
        int current = 0;

        count(legs, index, current);
        System.out.println(counter);

    }

    private static int counter = 0;

    private static void count(int[] legs, int index, int current) {
        if (current == 17) {
            counter++;
            return;
        }

        if (current > 17 || index > legs.length - 1) {
            return;
        }

        count(legs, index, current + legs[index]);
        count(legs, index + 1, current);

    }

}
