package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ChangePi {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();

        System.out.println(changePi(input));
    }

    private static String changePi(String input) {
        if (input.equals("")) {
            return "";
        }

        if (input.length() >= 2 && input.substring(0, 2).equals("pi")) {
            return input.replace("pi", "3.14");
        } else {
            return input.substring(0,1) + changePi(input.substring(1));
        }
    }
}
