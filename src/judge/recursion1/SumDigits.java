package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class SumDigits {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(sumOfDigits(Integer.parseInt(userInput.readLine())));
    }

    private static int sumOfDigits(int number) {
        if (number == 0)
            return 0;

        return number % 10 + sumOfDigits(number / 10);
    }
}
