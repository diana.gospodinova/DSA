package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CountHi {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();

        System.out.println(countHi(input));
    }

    private static int countHi(String input) {
        if (input.length() == 1 || input.equals("")) {
            return 0;
        }

        if (input.substring(0, 2).equals("hi")) {
            return 1 + countHi(input.substring(2));
        } else {
            return countHi(input.substring(1));
        }
    }
}
