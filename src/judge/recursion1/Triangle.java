package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Triangle {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(calculateTriangleBlocks(Integer.parseInt(userInput.readLine())));
    }

    private static int calculateTriangleBlocks(int blocks) {
        if (blocks == 0)
            return 0;
        return blocks + calculateTriangleBlocks(blocks - 1);
    }
}
