package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Array6 {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        String[] input = userInput.readLine().split(",");
        int index = Integer.parseInt(userInput.readLine());
        System.out.println(containsSix(input, index));
    }

    private static boolean containsSix(String[] input, int index) {
        int length = input.length;
        if (index == length)
            return false;

        if (input[index].equals("6")){
            return true;
        }else{
            return containsSix(input,index + 1);
        }
    }
}
