package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Count8 {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(countOfEight(Integer.parseInt(userInput.readLine())));
    }

    private static int countOfEight(int number) {
        if (number == 0)
            return 0;

        if (number % 10 == 8 && (number / 10) % 10 == 8) {
            return 2 + countOfEight(number / 10);
        } else if (number % 10 == 8) {
            return 1 + countOfEight(number / 10);
        } else {
            return countOfEight(number / 10);
        }
    }
}