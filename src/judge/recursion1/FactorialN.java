package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class FactorialN {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int number = Integer.parseInt(reader.readLine());
        System.out.println(factorialRec(number));
    }

    private static int factorialRec(int n) {
        if (n == 1)
            return 1;

        return n * factorialRec(n - 1);
    }
}
