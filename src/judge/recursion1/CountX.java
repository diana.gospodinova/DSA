package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CountX {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();

        System.out.println(countX(input));
    }

    private static int countX(String input) {
        if (input.equals(""))
            return 0;

        if (input.substring(0, 1).equals("x")) {
            return 1 + countX(input.substring(1));
        } else {
            return 0 + countX(input.substring(1));
        }
    }
}
