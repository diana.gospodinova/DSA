package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class PowerN {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        int number = Integer.parseInt(userInput.readLine());
        int power = Integer.parseInt(userInput.readLine());
        System.out.println(powerOfNumber(number, power));
    }

    private static int powerOfNumber(int number, int power) {
        if (power == 0)
            return 1;

        return number * powerOfNumber(number,power - 1);
    }
}
