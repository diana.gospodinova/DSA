package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BunnyEars {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        int bunnies = Integer.parseInt(userInput.readLine());
        System.out.println(calculateBunnyEars(bunnies));
    }

    private static int calculateBunnyEars(int bunnies) {
        if (bunnies == 0)
            return 0;
        return 2 + calculateBunnyEars(bunnies - 1);
    }
}
