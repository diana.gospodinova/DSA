package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class BunnyEars2 {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(calculateBunnyEars(Integer.parseInt(userInput.readLine())));
    }

    private static int calculateBunnyEars(int bunnies) {
        if (bunnies == 0)
            return 0;

        if(bunnies % 2 != 0) {
            return 2 + calculateBunnyEars(bunnies - 1);
        }else{
            return 3 + calculateBunnyEars(bunnies - 1);
        }
    }
}
