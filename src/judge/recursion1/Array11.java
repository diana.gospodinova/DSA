package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Array11 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] input = reader.readLine().split(",");
        int index = Integer.parseInt(reader.readLine());

        System.out.println(countEleven(input, index));
    }

    private static int countEleven(String[] input, int index) {
        if(index == input.length){
            return 0;
        }

        if(input[index].equals("11")){
            return 1 + countEleven(input, index + 1);
        }else{
            return countEleven(input, index + 1);
        }
    }
}
