package judge.recursion1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Count7 {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(countOfSeven(Integer.parseInt(userInput.readLine())));
    }

    private static int countOfSeven(int number) {
        if (number == 0)
            return 0;

        if (number % 10 == 7) {
            return 1 + countOfSeven(number / 10);
        } else {
            return countOfSeven(number / 10);
        }
    }
}
