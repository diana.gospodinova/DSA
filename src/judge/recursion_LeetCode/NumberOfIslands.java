package judge.recursion_LeetCode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class NumberOfIslands {
    static public int numIslands(char[][] grid) {
        int count = 0;

        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] != '0') {
                    findIsland(grid, i, j);
                    count++;
                }
            }
        }

        return count;
    }

    private static void findIsland(char[][] matrix, int startRow, int startCol) {
        if (!isInRange(matrix, startRow, startCol))
            return;

        if (matrix[startRow][startCol] == '0')
            return;

        matrix[startRow][startCol] = '0';

        findIsland(matrix, startRow, startCol + 1);
        findIsland(matrix, startRow + 1, startCol);
        findIsland(matrix, startRow - 1, startCol);
        findIsland(matrix, startRow, startCol - 1);
    }

    private static boolean isInRange(char[][] matrix, int row, int col) {
        if (col > matrix[0].length - 1) {
            return false;
        }

        if (row > matrix.length - 1) {
            return false;
        }

        if (col < 0) {
            return false;
        }

        if (row < 0) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] userInput = reader.readLine().split(" ");
        int rows = Integer.parseInt(userInput[0]);
        int cols = Integer.parseInt(userInput[1]);
        char[][] matrix = new char[rows][cols];

        for (int i = 0; i < rows; i++) {
            String row = reader.readLine();
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = row.charAt(j);
            }
        }

        System.out.println(numIslands(matrix));
    }
}
