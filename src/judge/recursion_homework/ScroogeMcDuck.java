package judge.recursion_homework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ScroogeMcDuck {
    static int rows;
    static int cols;

    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        String[] matrixSize = userInput.readLine().split(" ");
        rows = Integer.parseInt(matrixSize[0]);
        cols = Integer.parseInt(matrixSize[1]);
        int[][] matrix = new int[rows][cols];
        int currRow = 0;
        int currCol = 0;

        for (int i = 0; i < rows; i++) {
            String[] row = userInput.readLine().split(" ");
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = Integer.parseInt(row[j]);
                if (matrix[i][j] == 0) {
                    currRow = i;
                    currCol = j;
                }
            }
        }

        matrix[currRow][currCol] = -1;
        countCoins(matrix, currRow, currCol);
    }

    static int newRow = 0;
    static int newCol = 0;
    static int coins = 0;
    static int maxValue = Integer.MIN_VALUE;

    private static void countCoins(int[][] matrix, int currRow, int currCol) {
        //Move left
        if (currCol - 1 > -1) {
            if (matrix[currRow][currCol - 1] > maxValue) {
                newRow = currRow;
                newCol = currCol - 1;
                maxValue = matrix[newRow][newCol];
            }
        }

        //Move right
        if (currCol + 1 < cols) {
            if (matrix[currRow][currCol + 1] > maxValue) {
                newRow = currRow;
                newCol = currCol + 1;
                maxValue = matrix[newRow][newCol];
            }
        }

        //Move up
        if (currRow - 1 > -1) {
            if (matrix[currRow - 1][currCol] > maxValue) {
                newRow = currRow - 1;
                newCol = currCol;
                maxValue = matrix[newRow][newCol];
            }
        }

        //Move down
        if (currRow + 1 < rows) {
            if (matrix[currRow + 1][currCol] > maxValue) {
                newRow = currRow + 1;
                newCol = currCol;
                maxValue = matrix[newRow][newCol];
            }
        }

        if (matrix[newRow][newCol] > 0) {
            matrix[newRow][newCol] = matrix[newRow][newCol] - 1;
            coins++;
            currRow = newRow;
            currCol = newCol;
            maxValue = -1;
            countCoins(matrix, currRow, currCol);
        } else {
            System.out.println(coins);
        }
    }
}
