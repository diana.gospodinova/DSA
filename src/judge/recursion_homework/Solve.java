package judge.recursion_homework;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.util.ArrayDeque;
import java.util.Deque;

public class Solve {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        String expression = userInput.readLine();

        solveExpression(expression);
    }

    static Deque<Character> parenthesisOccurrence;
    static Deque<Integer> parenthesisIndexes;

    private static void solveExpression(String expression) {
        if (!expression.contains(String.valueOf("("))) {
            System.out.println(evaluate(expression));
            return;
        }

        parenthesisOccurrence = new ArrayDeque<>();
        parenthesisIndexes = new ArrayDeque<>();

        for (int i = 0; i < expression.length(); i++) {
            char c = expression.charAt(i);
            if (c == '(') {
                parenthesisOccurrence.push(c);
                parenthesisIndexes.push(i);
            } else {
                if (c == ')' && parenthesisOccurrence.peek() == '(') {
                    int startIndex = parenthesisIndexes.peek();
                    int endIndex = i + 1;

                    expression = expression.replace(expression.substring(startIndex, endIndex),
                            String.valueOf(evaluate(expression.substring(startIndex+1, i))));
                    break;
                }
            }
        }
        solveExpression(expression);
    }

    static BigInteger evaluate(String s) {
        BigInteger result;
        BigInteger firstNumber;
        BigInteger secondNumber;
        String[] numbers;
        if (s.contains("+")) {
            numbers = s.split("\\+");
            firstNumber = new BigInteger(numbers[0].trim());
            secondNumber = new BigInteger(numbers[1].trim());

            result = firstNumber.add(secondNumber);
            return result;
        }

        if (s.contains("*")) {
            numbers = s.split("\\*");
            firstNumber = new BigInteger(numbers[0].trim());
            secondNumber = new BigInteger(numbers[1].trim());

            result = firstNumber.multiply(secondNumber);
            return result;
        }

        if (s.contains("--")) {
            BigInteger sign = new BigInteger(String.valueOf(1));
            if (s.startsWith("-")) {
                sign = new BigInteger(String.valueOf(-1));
                s = s.substring(1);
            }
            numbers = s.split("\\--");
            firstNumber = new BigInteger(numbers[0].trim()).multiply(sign);
            secondNumber = new BigInteger(numbers[1].trim()).multiply(new BigInteger(String.valueOf(-1)));

            result = firstNumber.subtract(secondNumber);
            return result;
        }

        if (s.contains("-")) {
            BigInteger sign = new BigInteger(String.valueOf(1));
            if (s.startsWith("-")) {
                sign = new BigInteger(String.valueOf(-1));
                s = s.substring(1);
            }
            numbers = s.split("\\-");
            firstNumber = new BigInteger(numbers[0].trim()).multiply(sign);
            secondNumber = new BigInteger(numbers[1].trim());

            result = firstNumber.subtract(secondNumber);
            return result;
        }

        result = new BigInteger(s);
        return result;
    }
}
