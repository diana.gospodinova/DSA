package judge.setandmap2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.util.*;

public class OnlineMarket {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String nextCommand;

        Comparator<Product> myComparator = Comparator.comparing(Product::getPrice).thenComparing(Product::getName);

        Set<String> productsNames = new HashSet<>();
        Map<String, TreeSet<Product>> productsByType = new HashMap<>();
        Set<Product> productsByPrice = new TreeSet<>(myComparator);
        StringBuilder result = new StringBuilder();

        String type;
        StringJoiner list;
        int count;
        double priceFrom;
        double priceTo;

        while (true) {
            nextCommand = reader.readLine();
            String[] commandParameters = nextCommand.split(" ");

            if (commandParameters.length == 4) {
                if (commandParameters[0].equals("add")) {
                    String name = commandParameters[1];
                    Double price = Double.parseDouble(commandParameters[2]);
                    type = commandParameters[3];

                    if (!productsNames.contains(name)) {
                        Product newProduct = new Product(name, price);
                        productsNames.add(name);

                        productsByType.putIfAbsent(type, new TreeSet<>(myComparator));
                        productsByType.get(type).add(newProduct);

                        productsByPrice.add(newProduct);
                        result.append(String.format("Ok: Product %s added successfully\n", name));
                        continue;
                    }
                    result.append(String.format("Error: Product %s already exists\n", name));
                    continue;
                } else {
                    type = commandParameters[3];
                    if (!productsByType.containsKey(type)) {
                        result.append(String.format("Error: Type %s does not exists\n", type));
                        continue;
                    }

                    list = new StringJoiner(", ");
                    count = 10;
                    result.append("Ok: ");
                    for (Product product : productsByType.get(type)) {
                        list.add(product.toString());
                        count--;
                        if (count < 1) {
                            break;
                        }
                    }
                    result.append(list.toString()).append("\n");
                    continue;
                }
            }

            if (commandParameters.length == 5) {
                if (commandParameters[3].equals("from")) {
                    priceFrom = Double.parseDouble(commandParameters[4]);
                    list = new StringJoiner(", ");
                    count = 10;
                    result.append("Ok: ");
                    for (Product value : productsByPrice) {
                        if (value.getPrice() >= priceFrom && count != 0) {
                            list.add(value.toString());
                            count--;
                        }
                    }
                    result.append(list.toString()).append("\n");
                    continue;
                } else {
                    priceTo = Double.parseDouble(commandParameters[4]);
                    list = new StringJoiner(", ");
                    count = 10;
                    result.append("Ok: ");
                    for (Product value : productsByPrice) {
                        if (value.getPrice() <= priceTo && count != 0) {
                            list.add(value.toString());
                            count--;
                        }
                    }
                    result.append(list.toString()).append("\n");
                    continue;
                }
            }

            if (commandParameters.length == 7) {
                priceFrom = Double.parseDouble(commandParameters[4]);
                priceTo = Double.parseDouble(commandParameters[6]);
                list = new StringJoiner(", ");
                count = 10;
                result.append("Ok: ");
                for (Product value : productsByPrice) {
                    if (value.getPrice() >= priceFrom && value.getPrice() <= priceTo && count != 0) {
                        list.add(value.toString());
                        count--;
                    }
                }
                result.append(list.toString()).append("\n");
                continue;
            }

            if (commandParameters[0].equals("end")) {
                System.out.println(result);
                break;
            }
        }
    }

    public static class Product {
        private String name;
        private double price;
        DecimalFormat df = new DecimalFormat("#.####");

        public Product(String name, double price) {
            this.name = name;
            this.price = price;
        }

        public String getName() {
            return name;
        }

        public double getPrice() {
            return price;
        }

        @Override
        public String toString() {
            return String.format("%s(%s)", name, df.format(price));
        }
    }
}