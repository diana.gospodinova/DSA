package judge.setandmap2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class PlayerRanking {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<Player> players = new ArrayList<>();
        int counter = 0;
        HashMap<String, TreeSet<Player>> playersByType = new HashMap<>();
        StringJoiner result;

        String name;
        String type;
        int age;
        int position;

        Comparator<Player> nameAscending = (o1, o2) -> o2.name.compareTo(o1.name);
        Comparator<Player> ageDescending = (o1, o2) -> Integer.compare(o2.age, o1.age);
        Comparator<Player> typeComparator = nameAscending.reversed().thenComparing(ageDescending);

        while (true) {
            String nextCommand = reader.readLine();
            if (nextCommand.equals("end")) {
                break;
            }
            String[] commandParameters = nextCommand.split(" ");
            String command = commandParameters[0];

            switch (command) {
                case "add":
                    name = commandParameters[1];
                    type = commandParameters[2];
                    age = Integer.parseInt(commandParameters[3]);
                    position = Integer.parseInt(commandParameters[4]);
                    Player newPlayer = new Player(name, type, age);
                    if (position <= counter) {
                        players.add(position - 1, newPlayer);
                    } else {
                        players.add(newPlayer);
                    }

                    playersByType.putIfAbsent(type, new TreeSet<>(typeComparator));
                    playersByType.get(type).add(newPlayer);

                    counter++;
                    System.out.println(String.format("Added player %s to position %s", name, position));
                    break;
                case "find":
                    type = commandParameters[1];
                    result = new StringJoiner("; ");
                    int count = 1;
                    if (playersByType.containsKey(type)) {
                        System.out.print(String.format("Type %s: ", type));
                        for (Player player : playersByType.get(type)) {
                            result.add(player.toString());
                            if (count == 5) {
                                break;
                            }
                            count++;
                        }
                        System.out.println(result.toString().trim());
                    } else {
                        System.out.println(String.format("Type %s: ", type));
                    }
                    break;
                case "ranklist":
                    int startPosition = Integer.parseInt(commandParameters[1]);
                    int endPosition = Integer.parseInt(commandParameters[2]);
                    result = new StringJoiner("; ");
                    if (counter < endPosition) {
                        endPosition = counter;
                    }

                    for (int i = startPosition - 1; i < endPosition; i++) {
                        result.add((i + 1) + ". " + players.get(i).toString());
                    }
                    System.out.println(result);
                    break;
            }
        }
    }

    public static class Player {
        String name;
        String type;
        int age;

        public Player(String name, String type, int age) {
            this.name = name;
            this.type = type;
            this.age = age;
        }

        @Override
        public String toString() {
            return String.format("%s(%d)", name, age);
        }
    }
}
