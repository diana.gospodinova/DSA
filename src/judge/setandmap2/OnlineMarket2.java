package judge.setandmap2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class OnlineMarket2 {
    // Fastest
    static class Product implements Comparable<Product> {
        private String productName;
        private double productPrice;

        public Product(String productName, double productPrice) {
            this.productName = productName;
            this.productPrice = productPrice;
        }

        public double getProductPrice() {
            return productPrice;
        }

        @Override
        public String toString() {
            if (productPrice == (int) productPrice) {
                return String.format("%s(%d)", productName, (int) productPrice);

            } else {
                return String.format("%s(%s)", productName, productPrice);
            }
        }

        @Override
        public int compareTo(Product product) {
            int priceCompare = Double.compare(this.productPrice, product.productPrice);
            if (priceCompare != 0) {
                return priceCompare;
            }
            return this.productName.compareTo(product.productName);
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

        HashSet<String> names = new HashSet<>();
        TreeSet<Product> productByPrice = new TreeSet<>();
        HashMap<String, TreeSet<Product>> productByType = new HashMap<>();

        StringJoiner list;
        int count;
        StringBuilder result = new StringBuilder();

        while (true) {
            String nextCommand = userInput.readLine();
            String[] commandParameters = nextCommand.split(" ");

            if (commandParameters[0].equals("add")) {
                if (names.contains(commandParameters[1])) {
                    result.append(String.format("Error: Product %s already exists\n", commandParameters[1]));
                    continue;
                }
                Product newProduct = new Product(commandParameters[1], Double.parseDouble(commandParameters[2]));

                names.add(commandParameters[1]);
                productByPrice.add(newProduct);
                productByType.putIfAbsent(commandParameters[3], new TreeSet<>());
                productByType.get(commandParameters[3]).add(newProduct);

                result.append(String.format("Ok: Product %s added successfully\n", commandParameters[1]));
                continue;
            }

            if (commandParameters.length == 4) {
                if (productByType.containsKey(commandParameters[3])) {
                    list = new StringJoiner(", ");
                    count = 10;
                    result.append("Ok: ");
                    for (Product product : productByType.get(commandParameters[3])) {
                        list.add(product.toString());
                        count--;
                        if (count < 1) {
                            break;
                        }
                    }
                    result.append(list.toString()).append("\n");
                    continue;
                }
                result.append(String.format("Error: Type %s does not exists\n", commandParameters[3]));
                continue;
            }

            if (commandParameters.length == 7) {
                list = new StringJoiner(", ");
                count = 10;
                result.append("Ok: ");
                for (Product product : productByPrice) {
                    if (product.getProductPrice() >= Double.parseDouble(commandParameters[4])
                            && product.getProductPrice() <= Double.parseDouble(commandParameters[6])) {
                        list.add(product.toString());
                        count--;
                        if (count < 1) {
                            break;
                        }
                    }
                }
                result.append(list.toString()).append("\n");
                continue;
            }

            if (commandParameters.length == 5 && commandParameters[3].equals("from")) {
                list = new StringJoiner(", ");
                count = 10;
                result.append("Ok: ");
                for (Product product : productByPrice) {
                    if (product.getProductPrice() >= Double.parseDouble(commandParameters[4])) {
                        list.add(product.toString());
                        count--;
                        if (count < 1) {
                            break;
                        }
                    }
                }
                result.append(list.toString()).append("\n");
                continue;
            }

            if (commandParameters.length == 5 && commandParameters[3].equals("to")) {
                list = new StringJoiner(", ");
                count = 10;
                result.append("Ok: ");
                for (Product product : productByPrice) {
                    if (product.getProductPrice() <= Double.parseDouble(commandParameters[4])) {
                        list.add(product.toString());
                        count--;
                        if (count < 1) {
                            break;
                        }
                    }
                }
                result.append(list.toString()).append("\n");
                continue;
            }

            if (commandParameters[0].equals("end")) {
                System.out.println(result);
                break;
            }
        }
    }
}