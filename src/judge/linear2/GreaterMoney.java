package judge.linear2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class GreaterMoney {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] bag1 = reader.readLine().split(",");
        String[] bag2 = reader.readLine().split(",");
        ArrayList<String> result = new ArrayList<>();
        String index = "";

        for (int i = 0; i < bag1.length; i++) {
            result.add(index);
            index = "-1";
            for (int j = 0; j < bag2.length - 1; j++) {
                if (bag1[i].equals(bag2[j])) {
                    for (int k = j + 1; k < bag2.length; k++) {
                        if (Integer.parseInt(bag1[i]) < Integer.parseInt(bag2[k])) {
                            index = bag2[k];
                            break;
                        }
                    }
                    break;
                }
            }
        }

        result.add(index);
        System.out.print(result.get(1));
        for (int i = 2; i < result.size(); i++) {
            System.out.print("," + result.get(i));
        }
    }
}