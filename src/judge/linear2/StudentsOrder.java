package judge.linear2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class StudentsOrder {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] input = reader.readLine().split(" ");
        int changes = Integer.parseInt(input[1]);
        ArrayList<String> students = new ArrayList<>(Arrays.asList(reader.readLine().split(" ")));

        int oldPosition;
        int newPosition;

        while (changes > 0) {
            String[] change = reader.readLine().split(" ");
            oldPosition = students.indexOf(change[0]);
            newPosition = students.indexOf(change[1]);
            students.add(newPosition, change[0]);

            if (oldPosition < newPosition) {
                students.remove(oldPosition);
            } else {
                students.remove(oldPosition + 1);
            }

            changes--;
        }

        for (String student : students) {
            System.out.print(student + " ");
        }
    }
}