package judge.training;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Handshakes {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(userInput.readLine());
        long[] solutions = new long[71];
        solutions[0] = 1;

        for (int allPeople = 2; allPeople <= n; allPeople++) {
            for (int oneSidePeople = allPeople - 2; oneSidePeople >= 0; oneSidePeople -= 2) {
                solutions[allPeople] +=
                        solutions[oneSidePeople]
                                *
                                solutions[allPeople - oneSidePeople - 2];

            }
        }

        System.out.println(solutions[n]);
    }
}


