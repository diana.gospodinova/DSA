package judge.linear1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CokiSkoki_TheFirst {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

        int buildings = Integer.parseInt(userInput.readLine());
        String[] buildingHeights = userInput.readLine().split(" ");

        int[] heights = new int[buildings];
        int jumps = -1;
        int maxJumps;
        int count;
        int heightestBuilding = -1;
        int maxNumber = 0;
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < buildings; i++) {
            heights[i] = Integer.parseInt(buildingHeights[i]);
            if (heightestBuilding < heights[i]) {
                heightestBuilding = heights[i];
                maxNumber = heightestBuilding;
                jumps++;
            }
        }

        count = 1;
        maxJumps = jumps;
        result.append(jumps + " ");

        while (count < buildings - 1) {
            jumps = -1;
            heightestBuilding = -1;

            for (int i = count; i < heights.length; i++) {
                if (heightestBuilding < heights[i]) {
                    heightestBuilding = heights[i];
                    jumps++;
                }

                if (heights[i] == maxNumber) {
                    break;
                }
            }

            if (jumps > maxJumps) {
                maxJumps = jumps;
            }

            result.append(jumps + " ");

            count++;
        }

        result.append("0");
        System.out.println(maxJumps);
        System.out.println(result.toString());
    }
}
