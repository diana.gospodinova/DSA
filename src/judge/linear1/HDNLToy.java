package judge.linear1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Deque;

public class HDNLToy {
    public static void main(String[] args) throws IOException {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(userInput.readLine());
        String indentation = "";
        Deque<String> indentations = new ArrayDeque<>();
        Deque<Character> letters = new ArrayDeque<>();
        Deque<Integer> numbers = new ArrayDeque<>();
        String input;

        char letter;
        int number;

        for (int i = 0; i < n; i++) {
            input = userInput.readLine();
            letter = input.charAt(0);
            number = Integer.parseInt(input.substring(1));

            if (!indentations.isEmpty()) {
                if (number > numbers.peek()) {
                    indentation += " ";
                    pushElement(indentation, indentations, letters, numbers, letter, number);
                } else if (number == numbers.peek()) {
                    pollElement(indentations, letters, numbers);

                    pushElement(indentation, indentations, letters, numbers, letter, number);
                } else {
                    pollElement(indentations, letters, numbers);
                    while (!numbers.isEmpty() && numbers.peek() >= number) {
                        pollElement(indentations, letters, numbers);
                        indentation = indentation.substring(1);
                    }
                    pushElement(indentation, indentations, letters, numbers, letter, number);
                }
            } else {
                pushElement(indentation, indentations, letters, numbers, letter, number);
            }
        }

        while (!indentations.isEmpty()) {
            pollElement(indentations, letters, numbers);
        }
    }

    private static void pollElement(Deque<String> indentations, Deque<Character> letters, Deque<Integer> numbers) {
        System.out.println(indentations.peek() + "</" + letters.peek() + numbers.peek() + ">");
        indentations.poll();
        letters.poll();
        numbers.poll();
    }

    private static void pushElement(String indentation, Deque<String> indentations, Deque<Character> letters, Deque<Integer> numbers, Character letter, int number) {
        indentations.push(indentation);
        letters.push(letter);
        numbers.push(number);
        System.out.println(indentations.peek() + "<" + letters.peek() + numbers.peek() + ">");
    }
}

