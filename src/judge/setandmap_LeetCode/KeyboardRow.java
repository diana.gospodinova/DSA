package judge.setandmap_LeetCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class KeyboardRow {
    static public String[] findWords(String[] words) {
        ArrayList<String> result = new ArrayList<>();

        HashSet<String> firstRow = new HashSet<>(Arrays.asList(new String[]{"Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P"}));
        HashSet<String> secondRow = new HashSet<>(Arrays.asList(new String[]{"A", "S", "D", "F", "G", "H", "J", "K", "L"}));
        HashSet<String> thirdRow = new HashSet<>(Arrays.asList(new String[]{"Z", "X", "C", "V", "B", "N", "M"}));


        for (String word : words) {
            String[] letters = word.split("");
            if (CheckWord(result, firstRow, word, letters)) continue;
            if (CheckWord(result, secondRow, word, letters)) continue;
            if (CheckWord(result, thirdRow, word, letters)) continue;

        }

        return result.toArray(new String[0]);
    }

    private static boolean CheckWord(ArrayList<String> result, HashSet<String> row, String word, String[] letters) {
        int checkedLetters = 0;
        if (row.contains(letters[checkedLetters].toUpperCase())) {
            checkedLetters = 1;
            while (checkedLetters < letters.length) {
                if (row.contains(letters[checkedLetters].toUpperCase())) {
                    checkedLetters++;
                } else {
                    return false;
                }
            }
            if (checkedLetters == letters.length) {
                result.add(word);
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        String[] words = {"Hello", "Alaska", "Dad", "Peace"};
        System.out.println(findWords(words));
    }
}
