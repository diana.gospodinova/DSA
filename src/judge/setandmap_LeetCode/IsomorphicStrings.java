package judge.setandmap_LeetCode;

import java.util.HashMap;

public class IsomorphicStrings {
    static public boolean isIsomorphic(String s, String t) {
        String[] firstString = s.split("");
        String[] secondString = t.split("");
        HashMap<String, Integer> firstByLetters = new HashMap<>();
        HashMap<String, Integer> secondByLetters = new HashMap<>();

        if (firstString.length != secondString.length) {
            return false;
        }

        firstByLetters.put(firstString[0], 0);
        secondByLetters.put(secondString[0], 0);

        for (int i = 1; i < firstString.length; i++) {
            if (!firstByLetters.containsKey(firstString[i]) && !secondByLetters.containsKey(secondString[i])) {
                firstByLetters.put(firstString[i], i);
                secondByLetters.put(secondString[i], i);
                continue;
            }

            if (firstByLetters.containsKey(firstString[i]) && !secondByLetters.containsKey(secondString[i])) {
                return false;
            }

            if (!firstByLetters.containsKey(firstString[i]) && secondByLetters.containsKey(secondString[i])) {
                return false;
            }

            if (firstByLetters.get(firstString[i])  == secondByLetters.get(secondString[i])) {
                firstByLetters.put(firstString[i], firstByLetters.get(firstString[i]));
                secondByLetters.put(secondString[i], secondByLetters.get(secondString[i]));
                continue;
            } else {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        String s = "paper";
        String t = "title";
        System.out.println(isIsomorphic(s, t));
    }
}
