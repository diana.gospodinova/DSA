package judge.setandmap_LeetCode;

import java.util.HashSet;

public class HappyNumber {
    //HashSet<Integer> set = new HashSet<>(); LeetCode
    static HashSet<Integer> set = new HashSet<>();

    static public boolean isHappy(int n) {
        if (n == 1) {
            return true;
        }

        if(!set.add(n)) {
            return false;
        }

        int sum = 0;

        while (n > 0) {
            int lastDigit = n % 10;
            sum = (int) (sum + Math.pow(lastDigit, 2));
            n = n / 10;
        }

        return isHappy(sum);
    }

    public static void main(String[] args) {
        int number = 10;
        System.out.println(isHappy(number));
    }


    /*class Solution {
        public boolean isHappy(int n) {
            if (n == 1) {
                return true;
            }

            if(n == 4 || n == 16 || n == 37 || n == 58 || n == 145 || n == 42 || n== 20)
                return false;

            int sum = 0;

            while (n > 0) {
                int lastDigit = n % 10;
                sum = (int) (sum + Math.pow(lastDigit, 2));
                n = n / 10;
            }

            return isHappy(sum);
        }
    }*/
}
