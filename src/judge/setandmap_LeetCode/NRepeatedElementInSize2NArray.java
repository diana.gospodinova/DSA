package judge.setandmap_LeetCode;

import java.util.HashSet;

public class NRepeatedElementInSize2NArray {
    static public int repeatedNTimes(int[] A) {
        HashSet<Integer> numbers = new HashSet<>();
        int target = 0;

        for (int i = 0; i < A.length; i++) {
            if(numbers.contains(A[i])){
                target = A[i];
                break;
            }

            numbers.add(A[i]);

        }
        return target;
    }

    public static void main(String[] args) {
        int[] numbers = {5,1,5,2,5,3,5,4};
        System.out.println(repeatedNTimes(numbers));
    }
}
