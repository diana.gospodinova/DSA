package judge.setandmap_LeetCode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

public class FindTheDifference {
    static public char findTheDifference(String s, String t) {
        Character result = Character.MIN_VALUE;
        ArrayList<String> firstSet = new ArrayList<>(Arrays.asList(s.split("")));
        ArrayList<String> secondSet = new ArrayList<>(Arrays.asList(t.split("")));

        for(int i = 0 ; i < firstSet.size(); i++){
            if(secondSet.contains(firstSet.get(i))) {
                secondSet.remove(String.valueOf(firstSet.get(i)));
            }
        }

        result = secondSet.get(0).charAt(0);

        return result;
    }

    public static void main(String[] args) {
        String s = "abcd";
        String t = "abcde";
        System.out.println(findTheDifference(s, t));
    }
}
