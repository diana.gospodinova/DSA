package judge.setandmap_LeetCode;

import java.util.ArrayList;
import java.util.HashMap;

public class UncommonWordsFromTwoSentences {
    static public String[] uncommonFromSentences(String A, String B) {
        ArrayList<String> result = new ArrayList<>();
        HashMap<String, Integer> words = new HashMap<>();

        String[] firstSentence = A.split(" ");
        String[] secondSentence = B.split(" ");
        for (String word : firstSentence) {
            if (words.containsKey(word)) {
                words.put(word, words.get(word) + 1);
            } else {
                words.put(word,1);
            }
        }

        for (String word : secondSentence) {
            if (words.containsKey(word)) {
                words.put(word, words.get(word) + 1);
            } else {
                words.put(word,1);
            }
        }

        for (String word : words.keySet()) {
            if(words.get(word) == 1){
                result.add(word);
            }
        }


        return result.toArray(new String[0]);
    }

    public static void main(String[] args) {
        String firstSentence = "s z z z s";
        String secondSentence = "s z ejt";
        System.out.println(uncommonFromSentences(firstSentence, secondSentence));
    }
}
