package judge.setandmap_LeetCode;

import java.util.Arrays;
import java.util.HashSet;

public class JewelsAndStones {
    static public int numJewelsInStones(String J, String S) {
        String[] values = J.split("");
        HashSet<String> jewels = new HashSet<>(Arrays.asList(values));
        String[] stones = S.split("");
        int count = 0;
        for (int i = 0; i < stones.length; i++) {
            if (jewels.contains(stones[i])) {
                count++;
            }
        }
        return count;
    }

    public static void main(String[] args) {
        String jewels = "a";
        String stones = "AA";
        System.out.println(numJewelsInStones(jewels, stones));
    }
}
