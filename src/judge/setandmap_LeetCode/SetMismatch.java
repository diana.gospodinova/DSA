package judge.setandmap_LeetCode;

import java.util.Arrays;
import java.util.HashSet;

public class SetMismatch {
    static public int[] findErrorNums(int[] nums) {
        HashSet<Integer> numbers = new HashSet<>();
        String[] existingNumbers = new String[nums.length];
        int[] result = new int[2];

        for (int i = 0; i < nums.length; i++) {
            if(!numbers.contains(nums[i])){
                existingNumbers[nums[i] - 1] = "Y";
                numbers.add(nums[i]);
                continue;
            }

            result[0] = nums[i];
        }

        result[1] = Arrays.asList(existingNumbers).indexOf(null) + 1;

        return result;
    }

    public static void main(String[] args) {
        //[3,2,3,4,6,5]
        int[] numbers = {3,3,1};
        //int[] numbers = {3,2,3,4,6,2};
        System.out.println(findErrorNums(numbers)[0]);
        System.out.println(findErrorNums(numbers)[1]);
    }
}
