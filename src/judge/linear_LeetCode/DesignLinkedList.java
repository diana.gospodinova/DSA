package judge.linear_LeetCode;

import java.util.ArrayList;

public class DesignLinkedList {
    private ArrayList<Integer> list;

    public DesignLinkedList() {
        this.list = new ArrayList<>();
    }

    /**
     * Get the value of the index-th node in the linked list. If the index is invalid, return -1.
     */
    public int get(int index) {
        if (index < 0 || index > list.size() - 1) {
            return -1;
        }
        return list.get(index);
    }

    /**
     * Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list.
     */
    public void addAtHead(int val) {
        list.add(0, val);
    }

    /**
     * Append a node of value val to the last element of the linked list.
     */
    public void addAtTail(int val) {
        list.add(val);
    }

    /**
     * Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted.
     */
    public void addAtIndex(int index, int val) {
        if (index > 0 && index < list.size()) {
            list.add(index, val);
        }

        if (index <= 0) {
        /*["MyLinkedList","addAtIndex","get","deleteAtIndex"]
        [[],[-1,0],[0],[-1]] - It's a bug to axxept negative indexes*/
            addAtHead(val);
        }

        if (index == list.size()) {
            addAtTail(val);
        }

    }

    /**
     * Delete the index-th node in the linked list, if the index is valid.
     */
    public void deleteAtIndex(int index) {
        if (index >= 0 && index < list.size()) {
            list.remove(index);
        }
    }


    public static void main(String[] args) {
        DesignLinkedList obj = new DesignLinkedList();
        obj.addAtIndex(-1, 0);
        System.out.println(obj.get(0));
        obj.deleteAtIndex(-1);


    }
}

