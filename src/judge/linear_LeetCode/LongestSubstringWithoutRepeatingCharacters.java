package judge.linear_LeetCode;

import java.util.*;

public class LongestSubstringWithoutRepeatingCharacters {
    static public int lengthOfLongestSubstring(String s) {
        //TODO Debug again
        //https://leetcode.com/problems/longest-substring-without-repeating-characters/
        Map<Character, Integer> currentIndexMap = new HashMap<>();
        int maxSequence = 0;
        int prevIndex = 0;

        if (s.length() == 0) {
            return 0;
        }

        for (int i = 0; i < s.length(); i++) {
            if (currentIndexMap.containsKey(s.charAt(i))) {
                prevIndex = Math.max(currentIndexMap.get(s.charAt(i)), prevIndex);
            }

            maxSequence = Math.max(maxSequence, i - prevIndex + 1);
            currentIndexMap.put(s.charAt(i), i + 1);

        }
        return maxSequence;
    }

    public static void main(String[] args) {
        String s = " ";
        System.out.println(lengthOfLongestSubstring(s));

    }
}
