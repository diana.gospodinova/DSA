package judge.linear_LeetCode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;

public class MaximumFrequencyStack {
    List<Stack<Integer>> bucket;
    HashMap<Integer, Integer> map;

    public MaximumFrequencyStack() {
        bucket = new ArrayList<>();
        map = new HashMap<>();
    }

    public void push(int x) {
        map.put(x, map.getOrDefault(x, 0) + 1);
        int freq = map.get(x);
        if (freq - 1 >= bucket.size()) {
            bucket.add(new Stack<>());
        }
        bucket.get(freq - 1).add(x);
    }

    public int pop() {
        int freq = bucket.size();
        int x = bucket.get(freq - 1).pop();
        if (bucket.get(freq - 1).isEmpty()) {
            bucket.remove(bucket.size() - 1);
        }

        map.put(x, map.get(x) - 1);
        if (map.get(x) == 0) {
            map.remove(x);
        }

        return x;
    }

    public static void main(String[] args) {
        MaximumFrequencyStack crazy = new MaximumFrequencyStack();
        crazy.push(5);
        crazy.push(5);
        crazy.push(7);
        crazy.push(5);
        crazy.push(4);
        crazy.pop();
    }
}
