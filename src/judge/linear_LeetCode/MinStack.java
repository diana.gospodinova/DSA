package judge.linear_LeetCode;

import java.util.ArrayDeque;
import java.util.Deque;

public class MinStack {
    private Deque<Integer> minStack;

    public MinStack() {
        this.minStack = new ArrayDeque<>();
    }

    public void push(int x) {
        minStack.push(x);
    }

    public void pop() {
        minStack.pop();
    }

    public int top() {
        return minStack.peek();
    }

    public int getMin() {
        int minValue = Integer.MAX_VALUE;
        for (int number : minStack) {
            if(number < minValue){
                minValue = number;
            }
        }
        return minValue;
    }

    public static void main(String[] args) {
        MinStack obj = new MinStack();
        obj.push(3);
        obj.push(6);
        obj.push(-15);
        obj.push(-5);
        obj.push(13);
        obj.push(7);
        obj.pop();
        int param_3 = obj.top();
        int param_4 = obj.getMin();
        System.out.println(param_3);
        System.out.println(param_4);
    }
}

/**
 * Your MinStack object will be instantiated and called as such:
 * MinStack obj = new MinStack();
 * obj.push(x);
 * obj.pop();
 * int param_3 = obj.top();
 * int param_4 = obj.getMin();
 */