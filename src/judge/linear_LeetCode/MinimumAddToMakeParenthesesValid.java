package judge.linear_LeetCode;

import java.util.*;

public class MinimumAddToMakeParenthesesValid {
    static public int minAddToMakeValid(String S) {
        String[] parenthesis = S.split("");
        Stack<String> result = new Stack<>();
        String openingParenthesis = "(";
        String closingParenthesis = ")";

        if(S.length() == 0){
            return 0;
        }

        if(S.length() == 1){
           return 1;
        }

        for (int i = 0; i < parenthesis.length; i++) {
            if(result.size() == 0){
                result.add(parenthesis[i]);
            }else if(parenthesis[i].equals(closingParenthesis)){
                if(result.peek().equals(openingParenthesis)){
                    result.pop();
                }else{
                    result.add(parenthesis[i]);
                }
            }else{
                result.add(parenthesis[i]);
            }
        }

        return result.size();
    }

    public static void main(String[] args) {
        String input = ")()";
        System.out.println(minAddToMakeValid(input));
    }
}
