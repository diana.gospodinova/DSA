package judge.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class MessageInABottle {
    static TreeMap<String, String> representations;
    static String secretCode;

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        secretCode = reader.readLine();
        String cipher = reader.readLine();
        representations = new TreeMap<>();
        String value = "";
        String key = "";
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < cipher.length(); i++) {
            if (Character.isDigit(cipher.charAt(i))) {
                key = key + cipher.charAt(i);
            } else {
                if (value.length() != 0) {
                    representations.put(key, value);
                    value = "";
                    key = "";
                }
                value = Character.toString(cipher.charAt(i));
            }
        }

        representations.put(key, value);

        decodeMessage(0, result);

        System.out.println(results.size());
        Collections.sort(results);
        for (String message : results) {
            System.out.println(message);
        }
    }

    static ArrayList<String> results = new ArrayList<>();

    private static void decodeMessage(int index, StringBuilder result) {
        if (index == secretCode.length()) {
            results.add(result.toString());
            return;
        }

        for (String key : representations.keySet()) {
            if (secretCode.substring(index).startsWith(key)) {
                result.append(representations.get(key));
                decodeMessage(index + key.length(), result);
                result.setLength(result.length() - 1);
            }
        }
    }
}