package judge.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class AandB3 {
    static int digits;

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        digits = Integer.parseInt(reader.readLine());
        String[] numbers = reader.readLine().split(" ");
        Arrays.sort(numbers);
        StringBuilder result = new StringBuilder();

        findCombinations(numbers, result);
    }

    private static void findCombinations(String[] numbers, StringBuilder result) {
        if (result.length() == digits) {
            System.out.println(result.toString());
            return;
        }

        for (String number : numbers) {
            result.append(number);
            findCombinations(numbers, result);
            result.setLength(result.length() - 1);
        }
    }
}
