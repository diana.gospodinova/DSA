package judge.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class MessageInABottle2 {
    static TreeMap<String, String> representations;

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String secretCode = reader.readLine();
        String cipher = reader.readLine();
        representations = new TreeMap<>();
        String values = "";
        String keys = "";
        ArrayList<String> keySet = new ArrayList<>();
        String result = "";

        for (int i = 0; i < cipher.length(); i++) {
            if (Character.isDigit(cipher.charAt(i))) {
                keys = keys + cipher.charAt(i);
            } else {
                if (values.length() != 0) {
                    representations.put(keys, values);
                    keySet.add(keys);
                    values = "";
                    keys = "";
                }
                values = Character.toString(cipher.charAt(i));
            }
        }

        representations.put(keys, values);
        keySet.add(keys);
        Arrays.sort(keySet.toArray());

        for (int i = 0; i < keySet.size(); i++) {
            if (secretCode.indexOf(keySet.get(i)) != 0) {
                continue;
            }
            decodeMessage(secretCode, secretCode.length(), keySet.get(i), result);
        }

        System.out.println(counter);
        for (String message : results) {
            System.out.println(message);
        }
    }

    static TreeSet<String> results = new TreeSet<>();
    static int counter = 0;
    static int keyLength = 0;

    private static void decodeMessage(String secretCode, int secretLength, String key, String result) {
        if (secretCode.startsWith(key)) {
            keyLength = key.length();
            secretLength = secretLength - keyLength;
            result = result + representations.get(key);
            secretCode = secretCode.substring(key.length());
        }

        if (secretLength == 0) {
            results.add(result);
            counter++;
            return;
        }

        for (String nextkey : representations.keySet()) {
            if (secretCode.startsWith(nextkey)) {
                decodeMessage(secretCode, secretLength, nextkey, result);
            }
        }
    }
}