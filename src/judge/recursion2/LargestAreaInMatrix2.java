package judge.recursion2;

import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStreamReader;

public class LargestAreaInMatrix2 {
    static int matrixSize;

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] userInput = reader.readLine().split(" ");
        int rows = Integer.parseInt(userInput[0]);
        int cols = Integer.parseInt(userInput[1]);
        String[][] matrix = new String[rows][cols];
        matrixSize = rows * cols;

        for (int i = 0; i < rows; i++) {
            String[] row = reader.readLine().split(" ");
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = row[j];
            }
        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                counter = 0;
                if (!matrix[i][j].equals("V")) {
                    findLargestArea(matrix, i, j, matrix[i][j]);

                    if (maxCount < counter) {
                        maxCount = counter;
                    }

                    if (matrixSize <= maxCount) {
                        System.out.println(maxCount);
                        return;
                    }
                }
            }
        }
    }

    private static int counter = 0;
    private static int maxCount = 0;
    private static int totalVisited = 0;

    private static void findLargestArea(String[][] matrix, int startRow, int startCol, String target) {
        if (startCol < 0) {
            return;
        }

        if (startRow < 0) {
            return;
        }

        if (startCol > matrix[0].length - 1) {
            return;
        }

        if (startRow > matrix.length - 1) {
            return;
        }

        if (!matrix[startRow][startCol].equals(target))
            return;

        counter++;
        matrixSize--;

        matrix[startRow][startCol] = "V";

        findLargestArea(matrix, startRow + 1, startCol, target);
        findLargestArea(matrix, startRow, startCol + 1, target);
        findLargestArea(matrix, startRow - 1, startCol, target);
        findLargestArea(matrix, startRow, startCol - 1, target);
    }
}
