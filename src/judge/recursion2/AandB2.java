package judge.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.TreeSet;

public class AandB2 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int digits = Integer.parseInt(reader.readLine());
        String[] numbers = reader.readLine().split(" ");

        temp.add(numbers[0]);
        temp.add(numbers[1]);
        result.add(numbers[0]);
        result.add(numbers[1]);

        findCombinations(digits, numbers[0], numbers[1]);

        for (String combination : result) {
            System.out.println(combination);
        }
    }

    static TreeSet<String> temp = new TreeSet<>();
    static TreeSet<String> result = new TreeSet<>();

    private static void findCombinations(int digits, String firstNumber, String secondNumber) {
        if(temp.first().length() == digits){
            return;
        }
        result.clear();
        for (String number : temp) {
            result.add(firstNumber + number);
            result.add(secondNumber + number);
        }
        temp = (TreeSet) result.clone();

        findCombinations(digits, firstNumber, secondNumber);
    }
}
