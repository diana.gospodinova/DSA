package judge.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Numerology {
    static int[] digits = new int[10];

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = reader.readLine();
        ArrayList<Integer> number = new ArrayList<>();

        for (int i = 0; i < input.length(); i++) {
            number.add(Character.getNumericValue(input.charAt(i)));
        }

        findDigit(number);

        for (int digit : digits) {
            System.out.print(digit + " ");
        }
    }

    private static void findDigit(ArrayList<Integer> number) {
        if (number.size() == 1) {
            digits[number.get(0)] = digits[number.get(0)] + 1;
            return;
        }

        for (int i = 0; i < number.size() - 1; i++) {
            int a = number.get(i);
            int b = number.get(i + 1);
            int temp = (a + b) * (a ^ b) % 10;
            number.set(i + 1, temp);
            number.remove(i);

            findDigit(number);

            number.set(i, a);
            number.add(i + 1, b);
        }
    }
}