package judge.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Deque;

public class LargestAreaInMatrix_BFS {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String[] userInput = reader.readLine().split(" ");
        int rows = Integer.parseInt(userInput[0]);
        int cols = Integer.parseInt(userInput[1]);
        String[][] matrix = new String[rows][cols];
        int matrixSize = rows * cols;
        Deque<Integer> queueRows = new ArrayDeque<>();
        Deque<Integer> queueCols = new ArrayDeque<>();
        int[] rowPositions = {0, 1, 0, -1};
        int[] colPositions = {1, 0, -1, 0};
        int currRow;
        int currCol;

        for (int i = 0; i < rows; i++) {
            String[] row = reader.readLine().split(" ");
            for (int j = 0; j < cols; j++) {
                matrix[i][j] = row[j];
            }
        }

        int counter;
        int maxCount = 0;

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                counter = 0;
                if (matrix[i][j] == "V") {
                    continue;
                }
                queueRows.offer(i);
                queueCols.offer(j);
                String key = matrix[i][j];
                matrix[i][j] = "V";
                counter++;
                matrixSize--;
                while (!queueRows.isEmpty()) {
                    for (int ii = 0; ii < rowPositions.length; ii++) {
                        currRow = rowPositions[ii] + queueRows.peek();
                        currCol = colPositions[ii] + queueCols.peek();
                        if (isInRange(matrix, currRow, currCol) && matrix[currRow][currCol].equals(key) && !matrix[currRow][currCol].equals("V")) {
                            queueRows.offer(currRow);
                            queueCols.offer(currCol);
                            matrix[currRow][currCol] = "V";
                            counter++;
                            matrixSize--;
                        }
                    }
                    queueRows.poll();
                    queueCols.poll();
                }

                if (maxCount < counter) {
                    maxCount = counter;
                }

                if (matrixSize <= maxCount) {
                    System.out.println(maxCount);
                    return;
                }
            }
        }
    }

    private static boolean isInRange(String[][] matrix, int row, int col) {
        if(col > matrix[0].length - 1){
            return false;
        }

        if(row > matrix.length - 1){
            return false;
        }

        if(col < 0){
            return false;
        }

        if(row < 0){
            return false;
        }

        return true;
    }
}
