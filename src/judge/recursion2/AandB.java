package judge.recursion2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Queue;

public class AandB {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int digits = Integer.parseInt(reader.readLine());
        String[] numbers = reader.readLine().split(" ");
        Arrays.sort(numbers);

        result.offer(numbers[0]);
        result.offer(numbers[1]);

        findCombinations(digits, numbers[0], numbers[1]);

        for (String combination : result) {
            System.out.println(combination);
        }
    }

    static Queue<String> result = new ArrayDeque<>();
    static Queue<String> temp = new ArrayDeque<>();

    private static void findCombinations(int digits, String firstNumber, String secondNumber) {
        if (result.peek().length() == digits) {
            return;
        }

        int size = result.size();
        for (int i = 0; i < size; i++) {
            result.offer(firstNumber + result.peek());
            temp.offer(secondNumber + result.peek());
            result.poll();
        }

        result.addAll(temp);
        temp.clear();
        findCombinations(digits, firstNumber, secondNumber);
    }
}
